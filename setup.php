<?php
/*
	ANKat setup
*/
$getting=[];
//root_path
$getting['root_path']=__DIR__;
//module path
$getting['md_path']="{$getting['root_path']}/module/";


//domain
$getting['domain']=$_SERVER['HTTP_HOST'];

//OS
$getting['os']=PHP_OS;

//ERROR LEVEL
$getting['display_errors']=ini_get('display_errors');

//post_max_size
$getting['post_max_size']=ini_get('post_max_size');
//upload_max_filesize
$getting['upload_max_filesize']=ini_get('upload_max_filesize');
//max_input_time
$getting['max_input_time']=ini_get('max_input_time');
?>
<html>
<head>
	<title>ANKat SETUP INFO</title>
</head>

<body>
	<h3>ANKat variables</h3>
	<h4>MD PATH</h4>
	<code><?php echo $getting['md_path'];?></code>
	<h4>ROOT PATH</h4>
	<code><?php echo $getting['root_path'];?></code>
	<h4>DOMAIN</h4>
	<code><?php echo $getting['domain'];?></code>
	<h4>OS</h4>
	<code><?php echo $getting['os'];?></code>
	<h4>display_errors</h4>
	<code><?php echo $getting['display_errors'];?></code>	
	<h4>post_max_size</h4>
	<code><?php echo $getting['post_max_size'];?></code>	
	<h4>upload_max_filesize</h4>
	<code><?php echo $getting['upload_max_filesize'];?></code>
	<h4>max_input_time</h4>
	<code><?php echo $getting['max_input_time'];?></code>
</body>
</html>