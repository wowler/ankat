<?php
/*
	anKat Framework init file
*/
include "module/ankat.php";
//if PHP SESSION ON
if(\ANK_CONF::get('PHP_SESSION')=='ON'){session_start();}

\ANK::ERROR_DISPLAY();
date_default_timezone_set(\ANK_CONF::get('timezone'));

//if set request record
if(\ANK::$rq_rec->status()==true){
	\ANK::$rq_rec->set_fronend_data();
	$ank_request_id=\ANK::$rq_rec->get_uid();
	header("ank-request-id: {$ank_request_id}");
}

//for cache load start
if(\ANK::$input->get('cache_flush')=='true')$cache_flush=true;
$cache_data=\ANK::$cache->page->load($cache_flush);
if($cache_data!==false){
	echo $cache_data;
	return true;
}
//LOAD autoloads
\ANK::$loader->autoload([
	ROOT_PATH."module/core/autoloads.php"
]);
//for cache load end
ob_start();
//load config
//global function & variable
include ROOT_PATH.'module/config/global.php';
//ROUTE
include ROOT_PATH.'module/config/routes.php';
\ANK::$route->load_urls();


$all_html=ob_get_clean();

//if set js setting to HTML bottom
if(\ANK_CONF::get('output_js_setting')==true){
	//write ANK basic setting
	$all_html.="<script>var ANK_SETTING=".json_encode(\ANK::make_basic_setting())."</script>";
}


if(\ANK::$cache->page->make_status===true){
	\ANK::$cache->page->make_cache_data($all_html);
}
echo $all_html;
if(\ANK::$rq_rec->status()===true){
	\ANK::$rq_rec->set_backend_data($all_html)->record_requested();
}