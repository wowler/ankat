<?php
/*
	server AJAX MAIN
	ver 0.9
*/
$require_path=__DIR__.'/module/ankat.php';
include $require_path;
//if PHP SESSION ON
if(\ANK_CONF::get('PHP_SESSION')=='ON'){session_start();}

\ANK::ERROR_DISPLAY();
date_default_timezone_set(ANK_CONF::get('timezone'));

//LOAD autoloads
\ANK::$loader->autoload([
	ROOT_PATH."module/core/autoloads.php"
]);

$ext_path=ROOT_PATH.'/module/ext/';//compile

$user=ANK::$input->get('user');
$file_site=ANK::$input->get('site');
$md_name=ANK::$input->get('md');

if(empty($user)){
	$user='_main';
}

$ct_url=$ext_path.$user.'/'.$md_name.'/'.$file_site;


if(!file_exists($ct_url)){//如果檔案不存在直接跳ERR
	echo 'no file set!';
	exit;
}

//default set ajax data = input_ajax_data and set direct call variable
$data = \ANK::$input->post('data');
$data['ajax_call_type']='ajax';

include $ct_url;
?>