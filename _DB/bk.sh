#!/bin/sh
#setting
source ./bk_config.sh

backup_fun(){
	filename=${db_name}"-bk";
	echo "Choice output type"
	echo "1)Only structure"
	echo "2)Structure AND Datas [default]"
	read -p "What output type do you wish?" ip
    case ${ip} in
        [1]* ) output_var="-d";filename=${filename}"-st";;
        [2]* ) ;;
        []* ) output_var="";;
    esac
	echo "Please input password for DB System"
	#NOW=$(date +"%Y%dm-%H%M")
	mysqldump -h${site} ${output_var} -u${user} -p ${db_name} > ${filename}".sql";
	
}

restore_fun(){
	if [ "${restore_type}" == 'remote' ];then
		site=${remote_site}
		printf "\E[0;34;43m"
		printf "WARNING!!"
		printf "\E[0m"
		
		printf "WILL RESTORE TO "
		printf "\E[0;31;47m"
		printf "REMOTE SITE"
		printf "\E[0m"
		printf "!!!"
		printf "\n"
	fi
	
	default_name=${db_name}"-bk"
	echo "Input filename (.sql no need input)"
	echo "default name is $default_name"
	echo "if you wish default name then ENTER with empty"
	read -p ">>" resotre_filename
	if [ ! "${resotre_filename}" ];then
	   resotre_filename=${default_name}
	fi
	echo "Please input password for DB System"
	
	mysql -h${site} -u${user} -p ${db_name} < ${resotre_filename}".sql"
}

echo "LINUX DB BACKUP AND RESTORE"
echo "Do you wish to backup or restore"
select choice in "backup" "restore" "restore_remote"; do
    case $choice in
        backup )
			backup_fun;
		break;;
        restore )
			restore_type="local";
			restore_fun;
		break;;
        restore_remote )
			restore_type='remote';
			restore_fun;
		break;;
    esac
done