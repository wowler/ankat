var ANK={
	'fun':{},
	'init':function(){
		this.btn_bind();
	},
	'btn_bind':function(){
		$('.lnk_btn').click(function(){
			var lnk=$(this).attr('href');

			location.href=lnk;

			var this_loc=window.location;
			var ref_url=this_loc['pathname']+this_loc['search'];

			ANK.fun.setcookie('ref_url',ref_url,1);
		});
		$(document).on('click','.ank_btn,.ank_click',function(){
			var method=$(this).attr('method');
			var target=$(this).attr('target');
			var action=$(this).attr('action');
			var args=$(this).attr('args');

			if(target=="next"){
				target=$(this).next();
			}
			if(target=="prev"){
				target=$(this).prev();
			}

			switch(method){
				case 'link':
					if(args=='_blank'){window.open(target);break;}
					if(args=='back'){window.history.back();break;}
					if(args=='forward'){window.history.forward();break;}
					location.href=target;
				break;
				case 'toggle':
					$(target).toggle();
				break;
				case 'show':
					$(target).show();
				break;
				case 'hide':
					$(target).hide();
				break;
				case 'alert':
					alert(args);
				break;
				case 'function':
					var fun_cal=$(this).attr('fun_cal');
					var args=$(this).attr('args');
					
					if(args=='this' || typeof(args)=='undefined')args=$(this);
					
					ANK._executeFunctionByName(fun_cal,window,args);
				break;
			}
			switch(action){
				case 'toggle':
					$(this).toggle();
				break;
				case 'show':
					$(this).show();
				break;
				case 'hide':
					$(this).hide();
				break;
			}
		});
		$(document).on('change','.ank_chg',function(){
			var fun_cal=$(this).attr('fun_cal');
			var args=$(this).attr('args');

			if(args=='this' || typeof(args)=='undefined')args=$(this);
			
			ANK._executeFunctionByName(fun_cal,window,args);
		});
		$(document).on('keyup','.ank_live',function(){
			ANK._ank_live($(this));
		});
	},
	'get_form_data':function(obj){
		if(typeof(obj)!='object')return false;
		var data={}
		obj.find('input,select,textarea').each(function(i,v){
			var t=$(v);
			var tagName=t.prop("tagName").toLowerCase();
			var val='';

			switch(tagName){
				case 'select':
					val=t.find('option:selected').val();
				break;
				case 'textarea':
					val=t.val();
				break;
				case 'input':
					var t2=t.attr('type');
					switch(t2){
						case 'radio':
							if(t.prop('checked')===true){
								val=t.val();
							}else{
								return true;
							}
						break;
						case 'checkbox':
							if(t.prop('checked')===true){
								var odata=data[t.attr('name')];

								if(!$.isArray(odata)){
									odata=[];
								}

								odata.push(t.val());
								val = odata;
							}else{
								val=data[t.attr('name')];
							}
						break;
						case 'text':
						default:
							val=t.val();
						break;
					}
				break;
			}

			var chk_inx=typeof(data[t.attr('name')]);
			if(chk_inx!='undefined'){
				if(chk_inx=='object'){
					data[t.attr('name')].push(val);
				}
				if(chk_inx=='string'){
					var td=data[t.attr('name')];
					data[t.attr('name')]=[td,val];
				}
			}else{
				data[t.attr('name')]=val;
			}
		});

		return data;
	},
	'ajax':function(post_site_arr,post_data,post_function,post_type){

		post_type = post_type || 'undefinded';

		var post_site=[];
		if(typeof(post_site_arr)=='object'){
			post_site=post_site_arr;
		}else{
			alert('ERR input array');
			return 0;
		}

		var ajax_get_type='json';//default

		if(post_type!='undefinded'){
			ajax_get_type=post_type;
		}

		var ajax_href='//'+ANK_SETTING['base_url']+'/ajax.php?site='+post_site[0]+'&md='+post_site[1]+'&user='+post_site[2];

		$.post(ajax_href,post_data,post_function,ajax_get_type);

		return this;
	},
	'get_img_b64':function(obj,callback){
		var filesSelected = obj;
		if (filesSelected.length > 0){
			var fileToLoad = filesSelected[0].files[0];
			var fileReader = new FileReader();
			fileReader.onload = function(fileLoadedEvent){
	            var re=fileLoadedEvent.target.result;
	            if(typeof(callback)=='function')callback(re);
	        };
			fileReader.readAsDataURL(fileToLoad);

			var re=fileReader.result;

			return true;
		}

		return false;
	},
	'_executeFunctionByName':function(functionName, context){
		var args = [].slice.call(arguments).splice(2);
		var namespaces = functionName.split(".");
		var func = namespaces.pop();
		for(var i=0;i<namespaces.length;i++){
			context = context[namespaces[i]];
		}

		return context[func].apply(context,args);
	},
	'_ank_live':function(obj){
		var target=obj.attr('target');
		var val=obj.val();
		$(target).filter('input,textarea').val(val);
		$(target).not('input,textarea,select').html(val);
	}
}
ANK.fun={
	'setcookie':function(cname,cvalue,exdays){
	    var d = new Date();
	    d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    var expires = "expires="+d.toUTCString();
	    document.cookie = cname + "=" + cvalue + "; " + expires;
	},
	'get_anchor':function(sign){
		var val=window.location.hash.replace("#","");
		if(typeof(sign)=='undefinded'){return val;}
		
		var td=val.split(sign);
		return td;
	},
	'get_path':function(){
		var paths=window.location.pathname.split('/');
		paths.shift();

		return paths;
	}
}
$(document).ready(function(){
	ANK.init();
})