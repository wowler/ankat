<?php
/*
	libary class
*/
class LIB{
	public function __construct(){
		$this->conf = include __DIR__.'/lib_conf.php';

		$this->lib_load = [];
	}
	public function enable($name_arr=array()){
		if(!is_array($name_arr)){
			$name_arr=[$name_arr];
		}
		$conf = $this->conf;
		$keys = array_keys($conf);

		for($i=0,$cnt=count($name_arr);$i<$cnt;$i++){
			$name = $name_arr[$i];
			if(in_array($name,$keys)){//if in include list
				$lib_load = $this->lib_load;
				if(!in_array($name, $lib_load)){//if not include 
					$inc = $conf[$name];
					$load_site = ROOT_PATH.'module/core/'.$inc[0];
					include_once $load_site;
					if(empty($inc[2]))$inc[2]=null;
					\ANK::$$name=new $inc[1]($inc[2]);
					
				}
			}
		}

		
	}
}
?>