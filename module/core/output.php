<?php
/*
	CORE OUTPUT
*/
class OUTPUT{
	public function __construct(){
		$this->buffer = new OUTPUT_BUFFER;
	}
	public function set_output($html=''){
		$this->output_html=$html;

		return $this;
	}	
	public function append_output($html=''){
		$this->output_html.=$html;

		return $this;
	}
	public function render(){
		$html=$this->output_html;

		echo $html;
	}
	public function redirect($redirect='',$sec=3000){//轉址
		/*
			redirect:
			sec: renmain second
		*/

		$html='<script>setTimeout(function(){location.href="'.$redirect.'"},'.$sec.');</script>';

		return $html;
	}
	public function ajax_render($site,$md_name='__DIRECTED__',$user='_main'){
		/*
			user:
			md_name:
			site:
		*/
		if($md_name=='__DIRECTED__'){
			$final_site=$site;
		}else{
			$final_site='//'.WEB_SITE.'/ajax.php?site='.$site.'&md='.$md_name.'&user='.$user;
		}
		
		$script=$final_site;

		echo $script;
	}
	public function js_render($site,$md_name='__DIRECTED__',$user='_main'){
		/*
			user:
			md_name:
			site:
		*/
		if($md_name=='__DIRECTED__'){
			$final_site=$site;
		}else{
			$final_site='//'.WEB_SITE.'/js.php?user='.$user.'&md='.$md_name.'&site='.$site;
		}

		$script='<script src="'.$final_site.'"></script>';

		echo $script;
	}	
	public function css_render($site,$md_name='__DIRECTED__',$user='_main'){
		/*
			user:
			md_name:
			site:
		*/
		if($md_name=='__DIRECTED__'){
			$final_site=$site;
		}else{
			$final_site='//'.WEB_SITE.'/css.php?user='.$user.'&md='.$md_name.'&site='.$site;
		}

		$script='<link rel="stylesheet" href="'.$final_site.'" media="screen" type="text/css" />';

		echo $script;
	}
/*
	others
*/
	public function new_ajax($options=[]){
		/*
			debug
			type:
		*/
		$ajax = new class() extends \AJAX{};

		$debug='auto';
		if(isset($options['debug'])){
			$debug=$options['debug'];
		}
		
		$ajax->ajax_debug($debug);

		$type='json';
		if(isset($options['type'])){
			$type=$options['type'];
		}
		
		$ajax->ajax_set_type($type);

		return $ajax;
	}
}

class OUTPUT_BUFFER{
	public $data,$stream;
	public function __construct(){
		$this->data=[];
		$this->stream=[];
	}
	public function stack($type,$arr){
		if(gettype($this->data[$type])!='array'){
			$this->data[$type]=[];
		}
		$this->data[$type][]=$arr;


		return $this;
	}
	public function output($type){
		$tmp=$this->data[$type];

		$html="\n";

		$base_site='//'.WEB_SITE;

		if(!is_array($tmp))return ;

		foreach($tmp as $inx =>$val){
			$tmp_html='';
			switch($type){
				case 'js':
					$tmp_html='<script src="'.$base_site.'/js.php?site='.$val[0].'&md='.$val[1].'&user='.$val[2].'"></script>';
				break;
				case 'css':
					$tmp_html='<link rel="stylesheet" href="'.$base_site.'/css.php?site='.$val[0].'&md='.$val[1].'&user='.$val[2].'" media="screen" type="text/css" />';
				break;
			}

			$html.=$tmp_html."\n";
		}

		//if stream has script [JS]
		if($type=='js' && count($this->stream['js'])>0){
			$html.="<script>\n".implode("\n",$this->stream['js'])."\n</script>";
		}
		//if stream has script [CSS]
		if($type=='css' && count($this->stream['css'])>0){
			$html.="<style>\n".implode("\n",$this->stream['css'])."\n</style>";
		}
		

		return $html;
	}
	public function stream($type,$val1=null,$val2=null){
		/*
			type: js | css
			val1:
			val2:
		*/
		if(!is_null($val2)){//for stream data
			switch($type){
				case 'js':
					$script="var {$val1}=".json_encode($val2);
				break;
				case 'css':
					$script="{$val1}{{$val2}}";
				break;
				default:
					$script="i dont know";
				break;
			}
		}

		if(!is_null($val1) && is_null($val2)){//for stream script
			$script=$val1;
		}

		if(gettype($this->stream[$type])!='array'){
			$this->stream[$type]=[];
		}


		$this->stream[$type][]=$script;

		return $this;
	}
	public function clear($type){
		$this->data[$type]=[];

		return $this;
	}
}
?>