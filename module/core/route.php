<?php
/*
	ROUTE CORE
*/
class ROUTE{
	public $data,$error;
	public $init;
	public function __construct(){
		$this->init=false;
		//storage urls
		$this->urls=[];
	}
	//set datas
	public function set($url,$fun){
		/*

		*/
		//check left or righ has '/' if yes then remove
		//left /
			if(substr($url,0,1)=='/')$url=substr($url,1);
		//right /
			if(substr($url,-1)=='/')$url=substr($url,0,-1);
		//if strlen is 0
			if(strlen($url)==0)$url='__DEFAULT__';
		$this->urls[$url]=$fun;

		return $this;
	}
	//load set datas
	public function load_urls(){
		$this->output_status=true;
		$urls=$this->urls;
		$url_inx=array_keys($urls);

		if($this->init!=true){
			$this->itn_prc();
		}

		//get now urls
		$now_url=$this->data['basic'];

		$no_match=true;
		//split setting urls
		foreach($url_inx as $inx => $val){
			//split set urls by '/'
			$t=explode('/',$val);

			//if level 1 is match then pass to this function
			if($t[0]==$now_url[0]){
				array_shift($now_url);

				$this->args_data=$now_url;//set url extra datas

				$ank_callback=call_user_func_array($urls[$val],$now_url);
				$no_match=false;
				break;
			}
		}

		if($no_match===true){
			$this->args_data=$now_url;//set url extra datas
			//if no match run defalut
			$ank_callback=call_user_func_array($urls['__DEFAULT__'], $now_url);
		}

		//if finish then return 
		if($ank_callback===true){
			return true;
		}
		
		//check if is not array
		if(!is_array($ank_callback)){
			$ank_callback=[$ank_callback];
		}
		
		$this->ank_callback=$ank_callback;

		//if layout is false
		if($this->output_status!=false){
			$this->get_layout();
		}
		
	}
	//set layout file
	public function set_layout($url=''){
		$this->layout_path=$url;

		return $this;
	}
	//get layout file
	public function get_layout(...$input_arr){
		//if no data from agrus then get from temp data
		if(empty($input_arr)){
			$input_arr=$this->ank_callback;
		}
		
		$url=$this->layout_path;
		if(strlen($url)<1){//use default
			$url=ROOT_PATH.'module/layouts/'.\ANK_CONF::get('init_file')[0];
		}

		require $url;
	}
	//get temp data of url
	public function get_args(){
		$args=$this->args_data;//set url extra datas
		return $args;
	}
	//get datas
	public function get($type=''){
		if($this->init!=true){
			$this->itn_prc();
		}

		switch($type){
			case 'path':
				$re_data=$this->path;
			break;
			case 'all':
				$re_data=$this->data;
			break;
			case 'extend':
				$re_data=$this->data['extend'];
			break;
			case 'basic':
			default:
				$re_data=$this->data['basic'];
			break;
		}

		return $re_data;
	}
	//set layout is false
	public function set_output($status=true){
		$this->output_status=$status;

		return $this;
	}
	//proccess datas
	private function itn_prc(){
		//array data init
		$data=[
			'basic'=>[],
			'extend'=>[]
		];

		//basic variables proccess
		$handle=\ANK::$input->get('ank_handle');
		$handle=htmlspecialchars($handle);

		$tmp_handle=explode('/',$handle);
		for($i=0,$cnt=count($tmp_handle);$i<$cnt;$i++){
			$data['basic'][]=$tmp_handle[$i];
		}


		//proccess others variables
		$pr_others=htmlspecialchars($_SERVER['REQUEST_URI']);
		$cnt_pr_others=substr_count($pr_others,'?');

		if($cnt_pr_others==1){//if correct
			$tmp_others=explode('?',$pr_others);
			$tmp_others=explode('&amp;',$tmp_others[1]);
			for($i=0,$cnt=count($tmp_others);$i<$cnt;$i++){
				$t_str=$tmp_others[$i];
				$t_data=explode('=',$t_str);

				$data['extend'][$t_data[0]]=$t_data[1];
			}

		}else{//no result or errors
			if($cnt_pr_others>1){//if question sign counting over 1 is error
				$this->error='only one question sign can be founded';
				$data['extend']=false;
			}
		}

		$this->init=true;
		$this->data=$data;
		$this->path=$pr_others;

		return $this;
	}
}
?>