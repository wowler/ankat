<?php
/*
	LOG with DB
	ver 0.7a
*/
namespace ANK\LOG;

class LOG{
	public function __construct(){
		$this->db = \ANK::$db;
		$this->db_table = 'logs';//default table name
	}
	//set table name
	public function set_table($tb){
		$this->db_table=$tb;

		return $this;
	}
	public function GET($type=[],$v0=[],$v1=[],$v2=[],$ip=[]){
		/*
			[ARRAY TYPES]
			type:
			v0:
			v1:
			v2:
			ip:default
			timestamp:default
		*/
		$rd=$this->get_data($type,$v0,$v1,$v2,$ip);

		return $rd;
	}
	public function POST($type,$v0='',$v1='',$v2='',$ip='',$timestamp=''){
		/*
			type:
			v0:
			v1:
			v2:
			ip:default
			timestamp:default
		*/
		$re=$this->add_data($type,$v0,$v1,$v2,$ip,$timestamp);

		return $re;
	}
	/*
internal function
	*/
	//讀取資料
	private function get_data($type,$v0,$v1,$v2,$ip){
		/*
			type:arr[]
			v0:
			v1:
			v2:
			ip:
		*/
		$where_arr=[];

		if(!empty($type)<1){
			$where_arr[]='`type` IN ('.implode(',',$type).')';
		}
		if(!empty($v0)<1){
			$where_arr[]='`v0` IN ('.implode(',',$v0).')';
		}		
		if(!empty($v1)<1){
			$where_arr[]='`v1` IN ('.implode(',',$v1).')';
		}		
		if(!empty($v2)<1){
			$where_arr[]='`v2` IN ('.implode(',',$v2).')';
		}		
		if(!empty($ip)<1){
			$where_arr[]='`ip` IN ('.implode(',',$ip).')';
		}

		$sel_str="SELECT * FROM `{$this->db_table}`";
		if(count($where_arr)>0){
			$sel_str.=implode(' AND ',$where_arr);
		}

		$this->db->query($sel_str);
		$rd=[];
		while($r=$this->db->fetch_assoc()){
			$rd[]=$r;
		}

		return $rd;
	}
	//新增資料
	private function add_data($type,$v0,$v1,$v2,$ip,$timestamp){
		/*
			type:
			v0:
			v1:
			v2:
			ip:default
			timestamp:default
		*/

		if(strlen($v0)<1)$v0='';
		if(strlen($v1)<1)$v1='';
		if(strlen($v2)<1)$v2='';
		
		if(strlen($ip)<1)$ip=$this->ip_address_to_number($_SERVER['REMOTE_ADDR']);
		if(strlen($timestamp)<1)$timestamp=time();

		$ins_str="INSERT INTO `{$this->db_table}` (`type`,`v0`,`v1`,`v2`,`ip`,`timestamp`) VALUES 
					('".$type."','".$v0."','".$v1."','".$v2."','".$ip."','".$timestamp."');";

		$this->db->query($ins_str);

		$affected_rows=$this->db->affected_rows();
		if($affected_rows>0){
			$re = true;
		}else{
			$re = false;
		}

		return $re;
	}
	//change to current ip values
	private function ip_address_to_number($IPaddress) { 
		if(!$IPaddress) {
			return false;
		} else {
			$ips = explode('.',$IPaddress);
			return($ips[3] + $ips[2]*256 + $ips[1]*65536 + $ips[0]*16777216);
		}
	}
}
?>