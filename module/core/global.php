<?php
/*
	share var and functions
*/
class GL_VAR{
	public static $datas;
	public function __construct(){
		self::clean();
	}
	//show error
	public static function show(){
		
		return self::$datas;
	}
	//stack data
	public static function stack($data,$type='default'){
		
		if(gettype(self::$datas[$type])!='array'){
			self::$datas[$type]=array();
		}
		self::$datas[$type][]=$data;

		return self;
	}
	//set data
	public static function set($data,$type='default'){
		if(gettype($type)!=='string')return false;
		self::$datas[$type]=$data;

		return self;
	}
	public static function get($type='dfault'){
		return self::$datas[$type];
	}
	//clear data
	public static function clear(){
		self::$datas=array();

		return self;
	}
}

class GL_FUN{
	public static $data=array();
	public static function set($name,$obj){
		static::$data[$name]=$obj;
	}
	public static function __callStatic($name,$arguments){
		$obj=self::$data[$name];
		$t=$arguments;
		call_user_func_array($obj,$arguments);
	}
}
?>