<?php
/*
	CORE TOOLS ver 0.8a
	by jimpop 2015/11/29 01:46
*/

class TOOLS{
	//HTML REPLACE STRING
	public function html_replace($search,$replace,$content,$replace_target=false){
		$t=explode($search,$content);

		switch($replace_target){
			case false:
				$re=$t[0].$replace.$t[1];
			break;
			case 'adove':
				$re=$t[0].$search.$replace.$t[1];
			break;
			default:
				$re=$t[0].$replace.$search.$t[1];
			break;
		}

		return $re;
	}
	//get this page url
	public function get_current_url(){
		$pageURL = 'http';
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
		 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		} else {
		 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		}
		return $pageURL;
	}
}