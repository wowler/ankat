<?php
namespace ANK\DB;

class ORM{
	public function __construct($tb_name,$type_name=''){
		$this->clear();

		if(!is_null($tb_name)){
			$this->table($tb_name);
		}
		if($type_name=='raw'){
			$this->raw($tb_name);
		}
		$this->cache = false;
		$this->debug = false;
	}
	//RAW DB QUERY
	public function raw($query_string){
		//set no merge variable
		$this->no_merge_string=true;
		//set query string
		$this->query_str=$query_string;

		return $this;
	}
	//set basic DB table name
	public function table($enter_tb_name){
		//\ANK_LOG::stack(($this->tb_name),'tb_conunt');

		if(is_callable($enter_tb_name)){
			$inx=DB_SPACE::stack();
			
			$enter_tb_name(DB_SPACE::get($inx),$arguments);
			//if no set table "AS" name
			if(strlen(DB_SPACE::get($inx)->tb_as_name)<1){
				//auto set table "AS" name
				DB_SPACE::get($inx)->table_as();
			}
			$enter_tb_name=DB_SPACE::get($inx)->get_query_string();
		}

		if(is_string($enter_tb_name)){
			$this->tb_name[]=$enter_tb_name;
		}		

		return $this;
	}
	//set connection db
	public function db($enter_db_name){
		$this->select_db=$enter_db_name;

		return $this;
	}
	//get query string
	public function get_query_string(){
		$this->_merge_string();
		$re=$this->query_str;

		return $re;
	}
	//set debug is on
	public function debug($status=true){
		$this->debug=$status;

		return $this;
	}
	//set cache
	public function cache($status=true,$expire=600){
		$this->cache = true;
		$this->cache_expire = $expire;

		return $this;
	}
	//table as new table
	public function table_as($name='t1'){
		$this->tb_as_name=$name;

		return $this;
	}
	//set no where no execute
	public function where_empty_remove($status=false){
		/**/
		$this->where_empty_remove=$status;

		return $this;
	}
/*
	where area START
*/
	//set where AND string
	public function where($wh_arr=[],$fun=null){
		$arguments=array_slice(func_get_args(),2);

		$this->where_pre_ao('AND',$wh_arr,$fun,$arguments);

		return $this;
	}
	//set where OR string
	public function orwhere($wh_arr=[],$fun=null){
		$arguments=array_slice(func_get_args(),2);

		$this->where_pre_ao('OR',$wh_arr,$fun,$arguments);

		return $this;
	}
	//set where in
	public function wherein($col,$fun_or_arr=[],$empty_remove=false){
		if(empty($fun_or_arr)){
			if($empty_remove===true){return $this;}
			$this->where_pre_ao('AND',['1','0']);

			return $this;
		}
		$where_type='IN';
		$arguments=array_slice(func_get_args(),2);
		$this->where_pre_notin($where_type,$col,$fun_or_arr,$arguments);

		return $this;
	}
	//set where not in
	public function wherenotin($col,$fun_or_arr=[]){
		if(empty($fun_or_arr))return $this;
		$where_type='NOT IN';
		$arguments=array_slice(func_get_args(),2);
		$this->where_pre_notin($where_type,$col,$fun_or_arr,$arguments);

		return $this;
	}
	//set where between
	public function wherebetween($col,$val1,$val2){
		$rd=$this->parser_between($val1,$val2);

		$where_str='';
		$where_str.=$this->where_prefix_sign();

		$where_str.=" {$col} BETWEEN {$rd[0]} AND {$rd[1]}";

		$this->tb_where.=$where_str;

		return $this;
	}
	//set where not between
	public function wherenotbetween($col,$val1,$val2){
		$rd=$this->parser_between($val1,$val2);

		$where_str='';
		$where_str.=$this->where_prefix_sign();

		$where_str.=" {$col} NOT BETWEEN {$rd[0]} AND {$rd[1]}";

		$this->tb_where.=$where_str;

		return $this;
	}
/*
	where area END
*/
	//get datas and select from DB
	public function get($fun=null){
		$this->tb_type='select';
		$this->_merge_string();
		$this->_others_prepare();

		$query_str=$this->query_str;
		//make closure function for querying DB
		$get_data_fun=function() use ($query_str){
			$re=\ANK::$db->query($query_str);
			
			//if error
			if(!$re){
				$this->record_error_logs(['query'=>$query_str,'message'=>\ANK::$db->error()]);
			}

			$gd=[];
			while($r = \ANK::$db->fetch_assoc()){
				$gd[]=$r;
			}

			return $gd;
		};

		//if cachec is enabled
		if($this->cache){
			$re=\ANK::$cache->db->load($this->query_str);
			
			//if no cache or expired
			if(!$re){
				//require data from DB
				$re=$get_data_fun();

				//make cache data
				\ANK::$cache->db->make($re,$this->query_str,$this->cache_expire);
			}
			$gd=$re;
		}else{//if is diabled
			$gd=$get_data_fun();
		}
		
		$this->record_query_logs($this->query_str);

		if(is_callable($fun)){
			$arguments=array_slice(func_get_args(),1);
			
			$gd=$fun($gd,$arguments);
		}

		$this->clear();

		return $gd;
	} 
	//delete function
	public function delete(){
		$this->tb_type='delete';

		$this->_merge_string();
		$this->_others_prepare();

		if($this->debug){
			return $this->get_query_string();
		}

		$re=\ANK::$db->query($this->query_str);

		$this->record_query_logs($this->query_str);

		if($re){
			$rd=\ANK::$db->affected_rows();
		}else{
			$rd=false;
			$this->record_error_logs(['query'=>$this->query_str,'message'=>\ANK::$db->error()]);
		}

		$this->clear();

		return $rd;
	}
	//update data
	public function update($arr=[]){
		/**/
		$this->tb_type='update';

		$td=[];
		foreach($arr as $inx => $val){
			//if custom string
			if(mb_substr($val,0,5)=='@CUS@'){
				$val=mb_substr($val,5);
				$td[]="{$inx}={$val}";
			}else{
				$td[]="{$inx}='{$val}'";
			}
			
		}

		$tmp_str=implode(',',$td);

		$sd=[
			'upd_datas'=>$tmp_str
		];

		$this->_merge_string($sd);
		$this->_others_prepare();

		if($this->debug){
			return $this->get_query_string();
		}

		$re=\ANK::$db->query($this->query_str);

		$query_str=$this->query_str;
		if(strlen($query_str)>1024){
			$query_str=substr($query_str,0,1024).'......';
		}

		$this->record_query_logs($this->query_str);

		if($re){
			$rd=\ANK::$db->affected_rows();
		}else{
			$rd=false;
			$this->record_error_logs(['query'=>$this->query_str,'message'=>\ANK::$db->error()]);
		}

		$this->clear();

		return $rd;
	}
	//insert db string
	public function insert($arr=[],$more_arr=[]){
		$this->tb_type='insert';

		$td_inx=[];
		$td_datas=[];

		$chk_if_arr_is_assoc=function($arr){
			return count(array_filter(array_keys($arr),'is_string'))>0;
		};

		if($chk_if_arr_is_assoc($arr)){//if datas out to more_arr
			$empty_datas=false;
			foreach($arr as $inx => $val){
				$td_inx[]="`{$inx}`";
				$td_datas[]="'{$val}'";
			}
		}else{//if datas combined into more_arr
			$empty_datas=true;
			$td_inx=$arr;
		}

		

		if($empty_datas){//if no datas set in first step
			$tmp_str="(".implode(',',$td_inx).") VALUES ";
		}else{
			$tmp_str="(".implode(',',$td_inx).") VALUES (".implode(',',$td_datas).")";
		}
		

		if(count($more_arr)>0){//if inesrt more records
			$tmp_str2="";
			if(!is_array($more_arr[0])){
				$more_arr=[$more_arr];
			}
			foreach($more_arr as $data_inx){
				$tmp_more_d=[];
				foreach($data_inx as $val) {
					$tmp_more_d[]="'{$val}'";
				}
				$tmp_str2.=",(".implode(',',$tmp_more_d).")";
			}
			
			if($empty_datas){//if data combine to more_arr
				$tmp_str2=substr($tmp_str2,1);
			}
		}

		$tmp_str.=$tmp_str2;
		
		$sd=[
			'ins_datas'=>$tmp_str
		];

		$this->_merge_string($sd);
		

		if($this->debug){
			return $this->get_query_string();
		}

		$this->_others_prepare();
		$re=\ANK::$db->query($this->query_str);

		$query_str=$this->query_str;
		if(strlen($query_str)>1024){
			$query_str=substr($query_str,0,1024).'......';
		}

		$this->record_query_logs($this->query_str);

		if($re){
			$rd=\ANK::$db->affected_rows();
		}else{
			$rd=false;

			$this->record_error_logs(['query'=>$this->query_str,'message'=>\ANK::$db->error()]);
		}

		$this->clear();

		return $rd;
	}
	//select
	public function select($col=[]){
		$this->tb_type='select';

		$tmp_col=implode(',',$col);
		$this->tb_cols=$tmp_col;

		$this->_merge_string();
		$this->_others_prepare();

		return $this;
	}
/*
	EXTRA QUERY
*/
	//duplicate on key update
	public function duplicate($condition=[]){
		$td_inx=[];
		foreach($condition as $inx => $val){
			//if find ` sign then keep original string
			if(strpos($val,'`')!==false){
				$td_inx[]="`{$inx}`={$val}";
				continue;
			}
			$td_inx[]="`{$inx}`='{$val}'";
		}

		$t_str=implode(",",$td_inx);

		$this->tb_dulicate=" ON DUPLICATE KEY UPDATE {$t_str}";

		return $this;
	}
	//summary some column
	public function sum($select=''){
		/*
			EX:
				->sum('price')
		*/
		$select_str="SUM({$select}) AS `ANK_ORM_SUM`";
		$this->select([$select_str]);

		$rd=$this->get();

		//if multidata then give warning message
		if(count($rd)>1){
			$this->record_warning_logs([
				'query'=>$this->query_str,
				'message'=>'The querying result more than expected.expected count is 1'
			]);
		}

		//always single data
		$re=$rd[0]['ANK_ORM_SUM'];
	
		return $re;
	}
	//count some column
	public function count($select=''){
		/*
			EX:
				->count('price')
		*/
		if(strlen($select)<1)$select='*';
		$select_str="COUNT({$select}) AS `ANK_ORM_COUNT`";
		$this->select([$select_str]);

		$rd=$this->get();

		//if multidata then give warning message
		if(count($rd)>1){
			$this->record_warning_logs([
				'query'=>$this->query_str,
				'message'=>'The querying result more than expected.expected count is 1'
			]);
		}

		//always single data
		$re=$rd[0]['ANK_ORM_COUNT'];
	
		return $re;
	}
	//max some column
	public function max($select=''){
		/*
			EX:
				->max('price')
		*/
		$select_str="MAX({$select}) AS `ANK_ORM_MAX`";
		$this->select([$select_str]);

		$rd=$this->get();

		//if multidata then give warning message
		if(count($rd)>1){
			$this->record_warning_logs([
				'query'=>$this->query_str,
				'message'=>'The querying result more than expected.expected count is 1'
			]);
		}

		//always single data
		$re=$rd[0]['ANK_ORM_MAX'];
	
		return $re;
	}
	//min some column
	public function min($select=''){
		/*
			EX:
				->min('price')
		*/
		$select_str="MIN({$select}) AS `ANK_ORM_MIN`";
		$this->select([$select_str]);

		$rd=$this->get();

		//if multidata then give warning message
		if(count($rd)>1){
			$this->record_warning_logs([
				'query'=>$this->query_str,
				'message'=>'The querying result more than expected.expected count is 1'
			]);
		}

		//always single data
		$re=$rd[0]['ANK_ORM_MIN'];
	
		return $re;
	}
	//avg some column
	public function avg($select=''){
		/*
			EX:
				->avg('price')
		*/
		$select_str="AVG({$select}) AS `ANK_ORM_AVG`";
		$this->select([$select_str]);

		$rd=$this->get();

		//if multidata then give warning message
		if(count($rd)>1){
			$this->record_warning_logs([
				'query'=>$this->query_str,
				'message'=>'The querying result more than expected.expected count is 1'
			]);
		}

		//always single data
		$re=$rd[0]['ANK_ORM_AVG'];
	
		return $re;
	}
/*
	I U S AREA END
*/
	//orderby function
	public function orderby($odb_arr=[]){
		if(!is_array($odb_arr[0])){
			$odb_arr=[$odb_arr];
		}

		foreach($odb_arr as $inx => $val){
			$odb_tmp="{$val[0]} {$val[1]}";
			$this->tb_orderby[]=$odb_tmp;
		}

		return $this;
	}
	//limit function
	public function limit($start=0,$length=30){
		$this->tb_limit="{$start},{$length}";

		return $this;
	}
	//order function
	public function groupby($arr=[]){
		if(!is_array($arr)){
			$arr=[$arr];
		}

		$this->tb_groupby=$arr;

		return $this;
	}
	//having 
	public function having($agg_fun,$col,$oper,$val){
		$str="{$agg_fun}({$col}) {$oper} {$val}";
		$this->tb_having=$str;

		return $this;
	}
/*
	join area start
*/
	public function join($col,$condition){
		$this->join_pre_cal('JOIN',$col,$condition);

		return $this;
	}
	public function leftjoin($col,$condition){
		$this->join_pre_cal('LEFT JOIN',$col,$condition);

		return $this;
	}
	public function rightjoin($col,$condition){
		$this->join_pre_cal('RIGHT JOIN',$col,$condition);

		return $this;
	}
	//for sub query
	public function on($condition){
		$this->join_pre_on('AND',$condition);
				
		return $this;
	}
	public function oron($condition){
		$this->join_pre_on('OR',$condition);
		
		return $this;
	}
	//set query method
	public function method($str=''){
		$this->tb_method_string=$str;

		return $this;
	}
/*
	stack logs
*/
	//stack error logs
	public function record_error_logs($d=[]){
		/*
			query:
			message:
		*/
		\ANK_LOG::stack([
			'query'=>$d['query'],
			'message'=>$d['message'],
		],'DB_error');

		return $this;
	}
	//stack warning logs
	public function record_warning_logs($d=[]){
		/*
			query:
			message:
		*/

		\ANK_LOG::stack([
			'query'=>$d['query'],
			'message'=>$d['message'],
		],'DB_warning');
	}
	//stack query logs
	public function record_query_logs($d=[]){
		/*
			same as source data
		*/

		if(is_string($d) && strlen($d)>1024){
			$d=substr($d,0,1024).'......';
		}


		\ANK_LOG::stack($d,'DB_query');
	}
/*
	join area end
*/
/*
	internal function
*/
	//merge db string
	private function _merge_string($parm=[]){
		/*
			parm:[]
		*/
		//if set no merge querying then return 
		if($this->no_merge_string===true){
			return ;
		}
		$tb_type=$this->tb_type;
		$tb_name=$this->tb_name;
		$tb_where=$this->tb_where;
		$tb_orderby=$this->tb_orderby;
		$tb_limit=$this->tb_limit;
		$tb_cols=$this->tb_cols;
		$tb_groupby=$this->tb_groupby;
		$tb_having=$this->tb_having;
		$tb_join=$this->tb_join;
		$tb_as_name=$this->tb_as_name;

		$tb_method_string=$this->tb_method_string;

		$where_empty_remove=$this->where_empty_remove;

		//merge table name
		$tb_name_result=implode(",",$tb_name);

		$db_name=$this->select_db;
		$db_name_result='';
		if(strlen($db_name)>0){
			$db_name_result="{$db_name}.";
		}

		//merge join string
		$tb_join_result='';
		if(strlen($tb_join)>0){
			$tb_join_result=$tb_join;
		}

		$str='';
		switch($tb_type){
			case 'insert':
				$str="INSERT {$tb_method_string} INTO {$db_name_result}{$tb_name_result} {$parm['ins_datas']}";
				if($this->tb_dulicate!=false){
					$str.=$this->tb_dulicate;
				}
			break;
			case 'update':
				$str="UPDATE {$tb_method_string} {$db_name_result}{$tb_name_result} {$tb_join_result} SET {$parm['upd_datas']}";
			break;
			case 'delete':
				$str="DELETE {$tb_method_string} FROM {$db_name_result}{$tb_name_result} {$tb_join_result}";
			break;
			case 'select':
			default:
				if(strlen($tb_cols)<1){
					$tb_cols='*';
				}
				$str="SELECT {$tb_cols} FROM {$db_name_result}{$tb_name_result} {$tb_join_result}";
			break;
		}

		//merge all where string
		if(strlen($tb_where)>0 || $where_empty_remove==true){
			if(strlen($tb_where)<1)$tb_where="1=0";
			$where_str=" WHERE ".$tb_where;

			$str.=$where_str;
		}

		//merge groupby string
		if(count($tb_groupby)>0){
			$groupby_str=" GROUP BY ".implode(',',$tb_groupby);

			$str.=$groupby_str;
		}

		//merge having string
		if(strlen($tb_having)>0){
			$str.=" HAVING {$tb_having} ";
		}

		//merge all orderby string
		if(count($tb_orderby)>0){
			$orderby_str=" ORDER BY ".implode(',',$tb_orderby);

			$str.=$orderby_str;
		}

		//if limit exist
		if(strlen($tb_limit)>0){
			$str.=" LIMIT {$tb_limit}";
		}

		//if set table AS name
		if(strlen($tb_as_name)>0){
			$str="({$str}) AS ".$tb_as_name;
		}

		$this->query_str=$str;
	}
	//others prepare
	public function _others_prepare(){

		//if set db
		if(strlen($this->select_db)>0){
			\ANK::$db->select_db($this->select_db);
		}else{//set default db name
			$default_db_name=\ANK_CONF::get('db')['connection']['db_name'];
			\ANK::$db->select_db($default_db_name);
		}

		return $this;
	}
	//clear buffer space
	public function clear(){
		$this->tb_where='';
		$this->tb_cols='*';
		$this->tb_type='';
		//$this->tb_name=[];//maybe not delete
		$this->tb_limit='';
		$this->tb_having='';
		$this->tb_join='';
		$this->tb_as_name='';
		$this->tb_dulicate=false;

		$this->tb_orderby=[];
		$this->tb_groupby=[];

		$this->where_type='AND';
		$this->join_type='AND';

		$this->tb_method_string='';

		//$this->select_db='';//maybe not delete

		return $this;
	}
	//where where set prefix sign and insert it
	private function where_prefix_sign(){
		if(strlen($this->tb_where)>0){
			return ' '.$this->where_type.' ';
		}
		return '';
	}
	//where prepare
	private function where_pre_ao($type,$wh_arr,$fun=null,$arguments=[]){
		$this->where_type=$type;

		if(count($wh_arr)<1){
			return $this;
		}

		if(!is_array($wh_arr[0])){
			$wh_arr=[$wh_arr];
		}

		$where_str='';

		$where_str.=$this->where_prefix_sign();

		//where and combin
		$where_and_arr=[];
		foreach($wh_arr as $val){
			if(count($val)==2){
				$td="{$val[0]} = '{$val[1]}'";
				if($val[1]=='NULL')$td="{$val[0]} = NULL";
				$where_and_arr[]=$td;
				continue;
			}

			if($val[1]=='@table'){//if link with table
				$where_and_arr[]="{$val[0]} = {$val[2]}";
				continue;
			}

			if($val[3]=='@table'){//if link with table
				$where_and_arr[]="{$val[0]} {$val[1]} {$val[2]}";
				continue;
			}

			$td="{$val[0]} $val[1] '{$val[2]}'";
			if($val[2]=='NULL')$td="{$val[0]} $val[1] NULL";
			$where_and_arr[]=$td;
		}

		$implode_str=" {$type} ";
		$where_str.=implode($implode_str,$where_and_arr);

		if(is_callable($fun)){
			$inx=DB_SPACE::stack();
			
			$fun(DB_SPACE::get($inx),$arguments);
			$where_str.=' '.$type.' ('.DB_SPACE::get($inx)->tb_where.')';
		}

		$this->tb_where.=$where_str;

		return $this;
	}
	//where not in parser
	private function where_pre_notin($type,$col,$fun_or_arr=[],$arguments=[]){
		$where_str='';

		$where_str.=$this->where_prefix_sign();

		if(is_callable($fun_or_arr)){

			$inx=\DB_SPACE::stack();
			
			$fun_or_arr(\DB_SPACE::get($inx),$arguments);
			$where_str.=' '.$col.' '.$type.' ('.\DB_SPACE::get($inx)->get_query_string().')';
		}

		if(is_array($fun_or_arr)){
			//foreach add '' to string
			$arr=[];
			foreach($fun_or_arr as $key=> $val){
				if(is_string($val)){
					$arr[]="'{$val}'";
					continue;
				}
				$arr[]="{$val}";
			}
			$where_str.="{$col} {$type} (".implode(',',$arr).")";
		}
		

		$this->tb_where.=$where_str;

		return $this;
	}
	//join or sub query
	private function join_pre_on($type,$condition){
		$this->join_type=$type;
		$join_str='';

		if(strlen($this->tb_join)>0){
			$join_str.="{$this->join_str} {$this->join_type} ";
		}

		if(is_array($condition)){
			if(!is_array($condition[0])){
				$arr=[$condition];
			}

			//where and combin
			$where_or_arr=[];
			foreach($arr as $val){
				if(count($val)==2){
					$where_or_arr[]="{$val[0]} = {$val[1]}";
				}else{
					$where_or_arr[]="{$val[0]} {$val[1]} {$val[2]}";
				}
			}

			$implode_str=" {$type} ";
			$join_str.=implode($implode_str,$where_or_arr);
		}

		$this->tb_join.=$join_str;

		return $this;
	}
	//parser prepare join
	private function join_pre_cal($type,$col,$condition){
		$join_str='';
		if(is_array($col)){
			$key=array_keys($col)[0];
			$alias=" {$col[$key]}";
		}

		if(is_string($col)){
			$key=$col;
		}

		$head_str="{$key}{$alias}";

		if(is_callable($col)){
			$inx=DB_SPACE::stack();
			
			$col(DB_SPACE::get($inx));
			$table_as=DB_SPACE::get($inx)->tb_as_name;//set temp table as name
			if(strlen($table_as)<1){//if no set tables as
				$table_as=substr(md5(),0,6);//set a random table as name
			}
			DB_SPACE::get($inx)->tb_as_name='';//clear table as name
			$join_str.="(".DB_SPACE::get($inx)->get_query_string().") AS ".$table_as;
		}

		if(is_callable($condition)){
			$inx=DB_SPACE::stack();
			
			$condition(DB_SPACE::get($inx));
			$join_str.=" ON ".DB_SPACE::get($inx)->tb_join;
		}

		if(is_array($condition)){
			$arr=$condition;
			if(!is_array($condition[0])){
				$arr=[$condition];
			}

			//where and combin
			$where_and_arr=[];
			foreach($arr as $val){
				if(count($val)==2){
					$where_and_arr[]="{$val[0]} = {$val[1]}";
				}else{
					$where_and_arr[]="{$val[0]} {$val[1]} {$val[2]}";
				}
			}

			$implode_str=" AND ";
			$join_str.=" ON ".implode($implode_str,$where_and_arr);
		}

		if(is_string($condition)){
			$join_str.=" USING ({$condition})";
		}

		$join_str=" {$type} {$head_str}{$join_str}";

		$this->tb_join.=$join_str;

		return $this;
	}
	//parser between time
	private function parser_between($val1,$val2){
		$rd=[$val1,$val2];
		//if val1 and val2 are number
		if(is_numeric($val1) && is_numeric($val2)){
			if($val1 > $val2){//change order
				$rd=[$val2,$val1];
			}
		}

		return $rd;
	}
}

class DB_SPACE{
	public static $space;
	public static $inx;
	public static function init(){
		self::$space=[];
	}
	public static function get($inx){
		return self::$space[$inx];
	}
	public static function stack(){
		self::$space[]=\ANK::$db->new_space(null);
		$inx=count(self::$space)-1;

		return $inx;
	}
}
?>