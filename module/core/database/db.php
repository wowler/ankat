<?php
/*
	CORE FOR DB
*/
namespace ANK\DB;

include __DIR__.'/db_mysql.php';
include __DIR__.'/db_orm.php';

class DB{
	
	public $init_status;
	public $connection,$config;
	
	public function __construct(){
		
		$this->init_status=false;

		$this->point=-1;
		$this->init();

	}
	public function init(){
		$this->config=\ANK_CONF::get('db');
		
		$this->connection=array();

		$this->init_status=true;

		if($this->config['auto_start']==true){
			$this->make();
		}

		\ANK\DB\DB_SPACE::init();

		return true;
	}
	public function make($type='normal'){
		/*
			type:normal(default read) | read | write | other
		*/
		if(!$this->init_status){
			$this->init();
		}

		$db_driver=$this->config['connection']['driver'];
		$db_driver=strtoupper('db_'.$db_driver);

		$db_driver='\ANK\DB\\'.$db_driver;

		$db_obj=new $db_driver;
		$db_obj->connect($this->config['connection'],$type);

		$this->connection[]=$db_obj;

		$this->point=count($this->connection)-1;

		return $db_obj;
	}
	public function connection_count(){
		$point=$this->point;

		return $point;
	}
	public function sw($inx=0){
		$cnt=count($this->connection);

		if($inx>=0 && $inx <$cnt){
			$this->point=$inx;
			return $this;
		}else{
			return false;
		}
	}
	public function status(){//return if db connecting
		if($this->point==-1){
			$re=false;
		}else{
			$re=true;
		}

		return $re;
	}
	/*
		internal config connection
	*/
	public function __call($name, $arguments){
		if($name=='table' || $name=='new_space' || $name=='raw'){
			return new \ANK\DB\ORM($arguments[0],$name);
		}
		//if check connection true or false
		if($name=='query' || $name=='multi_query' || $name=='select_db'){
			if($this->connection_count()<1){
				$this->make();
			}
		}
        $point=$this->point;
        $re=$this->connection[$point]->$name($arguments[0]);

        return $re;
    }

}

/*
	this version no function for read and write
*/
abstract class DB_AB{
	public $connection,$db,$result;
	//check the syntax is read or write
	//###NOT IN USE NOW###
	public function ab_chk_rw($str){
		$str=trim($str);
		$str=substr($str,0,6);
		$type=strtolower($str);

		switch($type){
			case 'select':
				$re='read';
			break;
			case 'update':
			case 'insert':
			case 'delete':
				$re='write';
			break;
			default:
				$re='error';
			break;

		}

		return $re;
	}
	public function connect($config=array(),$type='normal'){
		/*
			host:
			user:
			password:
			db_name:
			charset:
		*/
		$this->prefix=$config['prefix'];
		$this->charset=$config['charset'];

		$sd=[
			'user'=>$config['user'],
			'password'=>$config['password']
		];

		switch($type){
			case 'write':
				$sd['host']=$config['write']['host'];
			break;
			case 'normal':
			case 'read':
			default:
				$sd['host']=$config['read']['host'];
			break;
		}
		
		$re=$this->ca_connect($sd);

		if(!$re){
			echo "Error: Could not connect to database.  Please try again later.";
			return false;
		}else{
			$this->connection=$re;
			$this->select_db($config['db_name']);
			$this->query("SET NAMES ".$config['charset']);
		}
		
		return $this;
	}
	public function select_db($db_name){
		$db_name=$this->prefix.$db_name;
		$re=$this->ca_select_db($db_name);

		if(!$re){
			echo "Error: Could not connect to db(table).  Please try again later.";
		}

		return $this;
	}
	public function query($str){
		$re=$this->ca_query($str);

		return $re;
	}
	public function multi_query($str){
		$re=$this->ca_multi_query($str);

		return $re;
	}
} 

interface DB_CLASS{
	public function ca_connect($config);
	public function ca_select_db($db_name);
	public function ca_query($str);
	public function ca_multi_query($str);
	public function error();
	public function fetch_assoc();
	public function fetch_array();
	public function fetch_row();
	public function num_rows();
	public function affected_rows();
	public function insert_id();
	public function result();
}
?>