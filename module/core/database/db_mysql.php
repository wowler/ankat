<?php	 	
/*
MYSQL CLASS for anKat by Jimpop 2016/04/28
multi connectiong
ver 0.8
*/
namespace ANK\DB;

class DB_MYSQL extends DB_AB implements DB_CLASS
{
	private $host,$user,$pass,$db_name;
	public function ca_connect($config=array()){
		/*
			host:{'read','write'}
			multi_server:true | false
			user:
			password:
			db_name:
			charset:
			collation:
			prefix:
		*/
		$host=$config['host'];
		$user=$config['user'];
		$password=$config['password'];

		$result=mysqli_connect($host,$user,$password);

		return $result;
	}
	public function ca_select_db($db_name){
		$re=mysqli_select_db($this->connection,$db_name);

		return $re;
	}
	public function ca_multi_query($str){
		$this->result=mysqli_multi_query($this->connection,$str);
		if($this->result)return true;
		else return false;
	}
	public function ca_query($str){
		$this->result=mysqli_query($this->connection,$str);
		if($this->result)return true;
		else return false;
	}
	public function fetch_array(){
		return mysqli_fetch_array($this->result);
	}
	public function fetch_assoc(){
		return mysqli_fetch_assoc($this->result);
	}
	public function fetch_row(){
		return mysqli_fetch_row($this->result);
	}
	public function num_rows(){
		@ $num=intval(mysqli_num_rows($this->result));
		$num=($num>0?$num:0);
		return $num;
	}
	public function affected_rows(){
		return mysqli_affected_rows($this->connection);
	}
	public function insert_id($res=0){
		return ($res==0?mysqli_insert_id($this->connection):mysqli_insert_id($this->connection,$res));
	}
	public function result($row=0,$field=0){
		$drow=$this->fetch_row();
		return $drow[$row][$field];
	}
	public function error(){
		return $this->connection->error;
	}
	public function real_escape_string($str){
		return mysqli_real_escape_string($this->result,$str);
	}
	/*

	*/
}
?>