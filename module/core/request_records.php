<?php
/*
	ANK REQUEST RECORDS LIB
*/

class RQ_REC{
	public function __construct(){
		$this->enabled=false;
		if(\ANK_CONF::get('request_record')===true){
			$this->enabled=true;//enable or disable records
		}
		
	}
	//enable function 
	public function enable(){
		$this->enabled=true;
	}
	//disable function 
	public function disable(){
		$this->enabled=false;
	}
	//get now switch status
	public function status(){
		return $this->enabled;
	}
	//set record frontend
	public function set_fronend_data(){
		$sd=[];
		//endpoint URL
		$domain=\ANK::$input->server('HTTP_HOST');
		$querying=\ANK::$input->server('REQUEST_URI');
		$sd['endpoint']=WEB_PATH.$querying;
		//method
		$sd['method']=\ANK::$input->server('REQUEST_METHOD');
		//query string
		$sd['query_string']=\ANK::$input->get();
		//post data
		$sd['post_data']=\ANK::$input->post();
		//raw data(phpinput)
		$sd['raw_input']=\ANK::$input->phpinput();
		//client(\ANK::$input->server();)
		$sd['client']=\ANK::$input->server();
		//client sending header
		$sd['client_headers']=apache_request_headers();
		//set ank track id
		$apa_uid=\ANK::$input->server('UNIQUE_ID');
		$uid=$this->make_uid_beautify(['uid'=>$apa_uid]);
		$this->uid=$uid;

		$this->preparing_data=$sd;

		return $this;
	}
	//set record backend
	public function set_backend_data($server_response){
		$sd=$this->preparing_data;

		//server response
		$sd['server_response']=$server_response;

		$this->preparing_data=$sd;

		return $this;
	}
	//reacrds requested
	public function record_requested(){
		$pd=$this->preparing_data;
	
		////get new id from DB
		$last_id=$this->get_new_id();
		////save to file

		//create folder(return folder path)
		$folder_path=$this->make_folder([
			'id'=>$last_id
		]);

		//file path
		$file_path="{$folder_path}{$last_id}.rec";

		$pendding_data=json_encode($pd,JSON_UNESCAPED_UNICODE);

		$re=\ANK::$file->put($file_path,$pendding_data);

		return $this;
	}
	//get new id from DB
	public function get_new_id(){
		// $apa_uid=\ANK::$input->server('UNIQUE_ID');
		// $uid=$this->make_uid_beautify(['uid'=>$apa_uid]);
		$uid=$this->uid;
		$method=\ANK::$input->server('REQUEST_METHOD');
		$uri=\ANK::$input->server('REQUEST_URI');
		$track_id=$Authorization=apache_request_headers()['ank-track-id'];

		$db=\ANK::$db->table('request_records');

		$re=$db->insert([
			'uid'=>$uid,
			'method'=>$method,
			'track_id'=>$track_id,
			'uri'=>$uri,
			'status'=>'1',
			'timestamp'=>time()
		]);

		$last_id=\ANK::$db->insert_id();

		return $last_id;
	}
	//get data
	public function get_uid(){
		return $this->uid;
	}
/*
	
*/
	private function make_folder($d=[]){
		/*
			id:
		*/
		//
		$id=$d['id'];

		$base_path=ROOT_PATH."/storage/request_records/";
		$folder_name=ceil($id/1000)*1000;

		$chk_path="{$base_path}{$folder_name}/";
		//if folder is not existed then make folder
		if(!\ANK::$file->file_exists($chk_path)){
			mkdir($chk_path);
		}

		return $chk_path;
	}
	private function make_link_hash($d=[]){
		/*
			id
			gid
		*/
		$t="{$d['id']}-{$d['gid']}";

		$r=substr(sha1($t),0,12);

		return $r;
	}
	private function make_uid_beautify($d=[]){
		/*
			uid:(from apache UNIQUE_ID)
		*/
		$uid=$d['uid'];
		$t=sha1($uid);
		$t1=substr($t,0,22);
		$t1=base_convert($t1,16,36);
		$t1=strtoupper($t1);
		$sign=chr(rand(65,90));
		$t1=str_pad($t1, 20, $sign, STR_PAD_LEFT);

		$rand=rand(1000,9999);

		$re="{$t1}-{$rand}";

		return $re;
	}
}
?>