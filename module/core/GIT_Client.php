<?php
/*
	GIT Client ver 0.8a
	by Jimpop 2015/09/30 15:46
*/
//about Git
class GIT_CLIENT{
	private $root_site,$ext_site;
	public $git_name,$ank_cfg;
	private $u_config,$u_site;
	public function __construct($git_name,$ank_cfg){
		$this->git_name = $git_name;

		$this->ank_cfg=$ank_cfg;

		$this->root_site = $ank_cfg['server']['root_path'];

		$this->ext_site = EXT_PATH;
		$this->u_site=EXT_PATH;//default user setting PATH
		
		return true;
	}
	public function gt($ut_name,$type='|na|',$user='|na|'){
		/*
			ut_name:
			type:
			user:
		*/
		$config=$this->ank_cfg;
		$u_site=$this->u_site;//default

		$rdu_type=($type=='|na|'?'':$type.'/');

		$git_name=$this->git_name;
		$ut_site='_main';
		if($user!='|na|'){
			$ut_site=$user;
		}else if($git_name!='_main'){
			$u_config=$this->u_config;
			$u_md_inx=array_keys($u_config['md_use']);

			if(in_array($type,$u_md_inx)){
				$ut_site=$u_config['md_use'][$type];
			}
		}
		
		$rdu_site=$ut_site.'/'.$rdu_type.$ut_name;
		$rd_site=$u_site.$rdu_site;

		return $rd_site;
	}
	//return user module site
	public function md_site($git_name='|na|',$md_name){
		/*
			git_name:
			md_name:
		*/
		if($git_name=='|na|'){
			$git_name='_main';
		}

		$rd_site=$git_name.'/'.$md_name;

		return $rd_site;
	}
	//auto parser user and module name
	public function parser_site($site){
		$ts=str_replace(EXT_PATH,'',$site);

		$ts2=explode('/',$ts);

		$rd=[
			'user'=>$ts2[0],
			'md'=>$ts2[1]
		];

		return $rd;
	}
}
?>