<?php
/*
	DEBUG STATCK
	ANK_LOG
*/
/*
 DEBUG::stack(array('dd'));
 echo DEBUG::show();

 ANK_LOG
*/

class DEBUG{
	public static $datas;
	public function __construct(){
		self::clean();
	}
	//show error
	public static function show(){
		
		return self::$datas;
	}
	//stack data
	public static function stack($data,$type='default'){
		
		if(gettype(self::$datas[$type])!='array'){
			self::$datas[$type]=array();
		}
		self::$datas[$type][]=$data;

		return self;
	}
	//set data
	public static function set($data,$type='default'){
		
		self::$datas[$type]=$data;

		return self;
	}
	//clear data
	public static function clear(){
		self::$datas=array();

		return self;
	}
}

class ANK_LOG{
	public static $datas,$types,$lv_datas;
	private static $record=true;
	public function __construct(){
		self::clean();
	}
	//set record or not
	public static function set_record($status=false){
		self::$record=$status;

		return self;
	}
	//display error logs
	public static function show(){
		$rd=self::$datas;
		if(count(self::$lv_datas)>0){
			$rd=array(
				'level'=>self::$lv_datas,
				'datas'=>self::$datas
			);
		}
		
		return $rd;
	}
	//stack data
	public static function stack($data,$group='default',$level=null){
		/*
			data
			group
			level:
		*/
		if(self::$record==false)return false;
		
		if(gettype(self::$datas[$group])!='array'){
			self::$datas[$group]=array();
		}
		self::$datas[$group][]=$data;

		//level
		if($level==null)return self;

		//level set datas
		if(gettype(self::$lv_datas[$level])!='array'){
			self::$lv_datas[$level]=array();
		}
		self::$lv_datas[$level][]=$data;

		return self;
	}
	//清除資料
	public static function clear(){
		self::$datas=array();
		self::$lv_datas=array();

		return self;
	}
}
?>