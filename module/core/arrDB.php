<?php
/*
	Array DB
*/
class ARRDB{
	public function __construct(){

	}
	public function make($data){
		return new ARRDB_CLASS($data);
	}
}
class ARRDB_CLASS{
	public function __construct($data){
		//ori datas
		$this->ori_data=$data;
		//proccess data
		$this->pd_data=$data;
		//select cols
		$this->select_cols=[];
		//title cols
		$this->title_cols=[];
		//set title or not
		$this->title_set=false;

		$this->init();
	}
	//init fucntion
	public function init(){
		//get every title
		$ori_data=$this->ori_data;

		$chk_if_assoc=function($arr){
		    if (array() === $arr) return false;
		    return array_keys($arr) !== range(0, count($arr) - 1);
		};

		//check if title is set
		$this->title_set=false;
		if($chk_if_assoc($ori_data[0])){
			$this->title_set=true;
		}
		
		$keys=array_keys($ori_data[0]);//get index 0
		
		$this->title_cols=$keys;

		return $this;
	}
	//reset to ori data
	public function reset(){
		//reset proccess data to ori data
		$this->pd_data=$this->ori_data;
		$this->select_cols=[];
		//title cols
		$this->title_cols=[];
		//set title or not
		$this->title_set=false;

		return $this;
	}
	//sort by orderby
	public function orderby($orderby_data=[]){
		/*
			orderby_data:[
				'id'=>'DESC',
				'col2'=>'ASC'
			]

			ori_data:[
				['id'=>1,'col2'=>'hi'],
				['id'=>2,'col2'=>'hi2']
			]
		*/
		//make orderby_inx
		$pr_ordby_inx=array_keys($orderby_data);

		$sort_fun=function($a,$b) use ($orderby_data,$pr_ordby_inx){
			$re='';
			foreach($pr_ordby_inx as $inx => $val){
				$odby=strtoupper($orderby_data[$pr_ordby_inx[$inx]]);
				if($odby=='ASC'){
					$re.=$a[$pr_ordby_inx[$inx]]-$b[$pr_ordby_inx[$inx]];
					continue;
				}
				$re.=$b[$pr_ordby_inx[$inx]]-$a[$pr_ordby_inx[$inx]];
			}

			return $re;
		};

		$pd_data=$this->pd_data;

		usort($pd_data,$sort_fun);

		$this->pd_data=$pd_data;


		return $this;
	}
	//
	public function where($cdt=[]){
		/*
			[
				['price','>',100],
				['name','jimpop'],
			]
		*/
		//
		if(!is_array($cdt[0])){
			$cdt=[$cdt];
		}

		$pd_data=$this->pd_data;
		$pr_data=[];

		foreach($pd_data as $inx => $val){

			//for check every condition
			$chk_result=$this->chk_condition($val,$cdt);

			//if every condition pass then push data
			if($chk_result===true)$pr_data[]=$val;
		}

		$this->pd_data=$pr_data;


		return $this;
	}
	//
	public function wherein($col='',$cdt=[]){
		/*
			col:
			cdt:[
				'value1','value2','value3'...
			]
		*/
		$pd_data=$this->pd_data;
		$pr_data=[];

		foreach($pd_data as $inx => $val){
			if(in_array($val[$col],$cdt))$pr_data[]=$val;
		}

		$this->pd_data=$pr_data;

		return $this;
	}
	//
	public function wherenotin($col='',$cdt=[]){
		/*
			col:
			cdt:[
				'value1','value2','value3'...
			]
		*/
		$pd_data=$this->pd_data;
		$pr_data=[];

		foreach($pd_data as $inx => $val){
			if(!in_array($val[$col],$cdt))$pr_data[]=$val;
		}

		$this->pd_data=$pr_data;

		return $this;
	}//
	public function groupby($cdt=[]){
		/*
			['price','name','jimpop']
		*/
		//
		$pd_data=$this->pd_data;
		//orderby first
		$ord_arr=[];
		foreach($cdt as $val){
			$ord_arr[]=[$val,'ASC'];
		}
		$this->orderby($ord_arr);

		$pr_data=[];

		$td_inx=[];
		$tkey_inx=[];

		foreach($pd_data as $inx => $val){
			$tmp_inx='';
			//every key index
			foreach($val as $key2 => $val2){
				if(in_array($key2,$cdt)){
					$tmp_inx.="{$val2}-";
				}
			}

			//if is not adding then insert
			if(!in_array($tmp_inx,$tkey_inx)){
				$td_inx[]=$val;
				$tkey_inx[]=$tmp_inx;
			}
			
		}

		$this->pd_data=$td_inx;

		return $this;
	}
	//
	public function select($sel=[]){
		/*

		*/
		$this->select_cols=$sel;

		return $this;
	}
	//
	public function insert($datas=[]){
		/*
			[
				[],[],[]
			]
		*/
		$pd_data=$this->pd_data;

		$pr_data=array_merge($pd_data,$datas);

		$this->pd_data=$pr_data;

		return $this;
	}
	//
	public function update($cdt=[],$chg_datas=[]){
		/*
			cdt:
			chg_datas:
		*/
		$pd_data=$this->pd_data;
		$pr_data=[];

		foreach($pd_data as $inx => $val){

			//for check every condition
			$chk_result=$this->chk_condition($val,$cdt);

			//if every condition pass then push data
			if($chk_result===true){
				//update cols
				foreach($chg_datas as $inx_chg => $val_chg){
					foreach($val_chg as $inx_chg2 => $val_chg2){
						$val[$inx_chg2]=$val_chg2;
					}
				}
			}
			$pr_data[]=$val;
		}

		$this->pd_data=$pr_data;

		return $this;
	}
	//
	public function delete($cdt=[]){
		/*
			cdt:
		*/
		$pd_data=$this->pd_data;
		$pr_data=[];

		foreach($pd_data as $inx => $val){

			//for check every condition
			$chk_result=$this->chk_condition($val,$cdt);

			//if every condition pass then skip
			if($chk_result===true){
				continue;
			}
			$pr_data[]=$val;
		}

		$this->pd_data=$pr_data;

		return $this;
	}
	//
	public function limit($start,$length){
		$this->limit=[$start,$length];

		return $this;
	}
	//
	public function leftjoin($merge_sdata=[],$onw=[],$whd=[]){
		/*
			merge_sdata:[
				[],[],[],[]
			]
			on_where:[
				['col','col'],// link table1 col to table2 col
				['col']//IF SAME THEN ONE ELEMENT
			]
			where:[
				['col','conditction'],
				['col','conditction'],
			]
		*/
		$pd_data=$this->pd_data;

		//if set where data
		if(is_array($whd) && count($whd)>0){
			//fill datas to merge_sdata
			foreach($merge_sdata as $key=> $val){
				foreach($whd as $key2 => $val2){
					$merge_sdata[$key][$val2[0]]=$val2[1];
				}	
			}
		}

		$re_data=$this->_join_data($pd_data,$merge_sdata,$onw,'LEFT');

		$this->pd_data=$re_data;

		return $this;
	}
	//
	public function rightjoin($merge_sdata=[],$onw=[],$whd=[]){
		/*
			merge_sdata:[
				[],[],[],[]
			]
			on_where:[
				['col','col'],// link table1 col to table2 col
				['col']//IF SAME THEN ONE ELEMENT
			]
			where:[
				['col','conditction'],
				['col','conditction'],
			]
		*/
		$pd_data=$this->pd_data;

		//if set where data
		if(is_array($whd) && count($whd)>0){
			//fill datas to merge_sdata
			foreach($merge_sdata as $key=> $val){
				foreach($whd as $key2 => $val2){
					$merge_sdata[$key][$val2[0]]=$val2[1];
				}	
			}
		}

		$re_data=$this->_join_data($pd_data,$merge_sdata,$onw,'RIGHT');

		$this->pd_data=$re_data;

		return $this;
	}
	//
	public function get(){
		$pd_data=$this->pd_data;

		$pr_data=[];

		//filter select cols
		$select_cols=$this->select_cols;
		//if set slect sols
		if(count($select_cols)>0){
			//for datas
			foreach($pd_data as $inx => $val){
				//for every unit inx && data
				foreach($val as $unit_inx => $unit_val){
					if(!in_array($unit_inx,$select_cols)){
						unset($val[$unit_inx]);
					}
				}
				$pr_data[]=$val;
			}

			return $pr_data;
		}

		return $pd_data;
	}
/*
	others function 
*/
	public function count(){
		$pd_data=$this->pd_data;
		$cnt=count($pd_data);

		return $cnt;
	}
	public function sum($col){
		/*
			sum which col
		*/
		$pd_data=$this->pd_data;

		$sum=0;
		foreach($pd_data as $inx => $val){
			if(is_numeric($val[$col])){
				$sum+=floatval($val[$col]);
			}
		}

		return $sum;
	
	}
	public function avg($col){
		/*
			sum which col
		*/
		$pd_data=$this->pd_data;

		$sum=0;
		$cnt=0;
		foreach($pd_data as $inx => $val){
			if(is_numeric($val[$col])){
				$cnt++;
				$sum+=floatval($val[$col]);
			}
		}

		$re=$sum/$cnt;

		return $re;
	}
	public function min($col){
		/*
			sum which col
		*/
		$pd_data=$this->pd_data;

		$min_fun=function($v1,$v2){
			if($v1>$v2)return $v2;
			return $v1;
		};

		$min=PHP_INT_MAX; 
		foreach($pd_data as $inx => $val){
			if(is_numeric($val[$col])){
				$min=$min_fun($min,$val[$col]);
			}
		}

		if($min===PHP_INT_MAX)return false;

		return $min;
	}
	public function max($col){
		/*
			sum which col
		*/
		$pd_data=$this->pd_data;

		$max_fun=function($v1,$v2){
			if($v1<$v2)return $v2;
			return $v1;
		};

		$max=-PHP_INT_MAX; 
		foreach($pd_data as $inx => $val){
			if(is_numeric($val[$col])){
				$max=$max_fun($max,$val[$col]);
			}
		}

		if($max===PHP_INT_MAX)return false;

		return $max;
	}
/*
	internal functions
*/
	private function chk_condition($val,$cdt){
		/*
			val:
			cdt:condition:[
				['index','<','value'],['index','value']
			]
		*/
		//for check every condition
		$chk_result=true;
		foreach($cdt as $cdt_inx => $cdt_val){

			switch($cdt_val[1]){
				case '>':
					if($val[$cdt_val[0]] <= $cdt_val[2])$chk_result=false;
				break;
				case '>=':
					if($val[$cdt_val[0]] < $cdt_val[2])$chk_result=false;
				break;
				case '<':
					if($val[$cdt_val[0]] >= $cdt_val[2])$chk_result=false;
				break;
				case '<=':
					if($val[$cdt_val[0]] > $cdt_val[2])$chk_result=false;
				break;
				//default is =
				case '=':
					$cdt_val[1]=$cdt_val[2];//swap pos 2 to pos 1
				default:
					if($val[$cdt_val[0]] != $cdt_val[1])$chk_result=false;
				break;
			}
		}

		return $chk_result;
	}
	//
	private function _join_data($source_data=[],$merge_sdata=[],$onw=[],$direction='LEFT'){
		/*

		*/
		//process same cols and source_arr priority first
		$mcols=[];
		foreach($source_data[0] as $key => $val){
			$mcols[]=$key;
		}

		//\DEBUG::set($mcols,'mcols');

		//for merge function
		$merge_fun=function($source_arr,$merge_arr,$mcols=[],$direction='LEFT'){
				

				//reduce same cols [source first]
			foreach($merge_arr as $inx => $val){
				if(in_array($inx,$mcols)){
					unset($merge_arr[$inx]);
				}
			}

			//\DEBUG::stack([$source_arr,$merge_arr],'merge_fun');
			
			if($direction=='LEFT'){
				$re_arr=array_merge($source_arr,$merge_arr);
				return $re_arr;
			}
			
			//else RIGHT
			$re_arr=array_merge($merge_arr,$source_arr);

			return $re_arr;
		};

		//for loop source data and merger data check
		foreach($source_data as $source_inx => $source_val){
			foreach($merge_sdata as $merge_inx => $merge_data){
				$cols_match=true;

				foreach($onw as $whd_inx => $whd_val){
					$cdt1=$whd_val[0];
					$cdt2=$whd_val[1];
					//if whd_val count is 1 then auto add to index 0 and 1
					if(strlen($cdt2)<1){
						$cdt2=$cdt1;
					}

					//\DEBUG::stack([$source_val[$cdt1],$merge_data[$cdt2],$source_val[$cdt1]==$merge_data[$cdt2]],'whd');

					//if col same
					if($source_val[$cdt1]==$merge_data[$cdt2]){
						$cols_match=true;
						continue;
					}

					//set match different
					$cols_match=false;
					break;
				}
				//\DEBUG::stack([$cols_match,$source_inx,$merge_inx],'aft_whd');
				if($cols_match==false)continue;

				//if all cols are match
					//merge data
					$re_val=$merge_fun($source_val,$merge_data,$mcols,$direction);
		
					$source_data[$source_inx]=$re_val;
				break;
			}
		}

		return $source_data;
	}
}
?>