<?php
/*
	CORE MVC 
*/
abstract class MVC_CONTROLLER{
	public function __construct(){

	}
}

abstract class MVC_MODEL{
	public function __construct(){

	}
}

abstract class MVC_VIEW{
	public function __construct(){

	}
}

//new feature,for global any module
class MD{
	public static $data=array('_default'=>null);
	public static $class_inx=array();
	public static $ns='_default';
	public static $stric=false;
	public static function use_stric($re=false){
		self::$stric=$re;
	}
	public static function by($ns='_default'){
		if(strlen($ns)>0 && $ns!='_default'){
			self::$ns = $ns;
		}
		
		return self::$ns;
	}
	public static function set($name,$obj,$ns='_default'){
		$rns=\MD::by($ns);

		if(!is_array(self::$data[$rns])){
			self::$data[$rns]=[];
		}
		self::$data[$rns][$name]=$obj;
		self::$class_inx[$name]=[$rns,$name];
	}
	public static function __callStatic($name, $arguments){
		if(is_array($arguments) && count($arguments)<1){
			$ns=self::$ns;
		}else{
			$ns=$arguments[0];
		}

		if(self::$stric==true){
			return self::$data[$ns][$name];
		}else{
			$inx=self::$class_inx[$name];
			return self::$data[$inx[0]][$inx[1]];
		}
		
		
	}
}

abstract class MD_SCOPE{
	public static $data;
	public static function set($val,$type='default'){
		static::$data[$type]=$val;
		
		return $this;
	}
	public static function stack($val,$type='default'){
		if(gettype(static::$data[$type])!='array'){
			static::$data[$type]=array();
		}
		static::$data[$type][]=$val;

		return $this;
	}
	public static function get($type=''){
		if(strlen($type)<1)return static::$data;
		else{
			return static::$data[$type];
		}
	}
}
?>