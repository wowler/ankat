<?php
/*
DB Session ver 0.1a
by Jimpop 2015/09/28 02:31:00

####IMPORTANT####
need MySQL Class
*****
$dbs=new DB_SESSION({timeout});

$dbs->new_session();
$dbs->load_session();
$dbs->del_session();
*/
namespace ANK\DB_SESSION;

class DB_SESSION{
	public $timeout;
	public $session_data,$session_status;
	public function __construct($timeout=10800){
		$this->set_timeout($timeout);
		$this->set_update_time=300;//default 5min
		$this->db=\ANK::$db;
		$this->init_status=true;
	}
	//set timeout for login session
	public function set_timeout($timeout){
		$timeout=intval($timeout);
		if($timeout<60 || $timeout>(86400*60)){
			$timeout=10800;
		}
		$this->timeout=$timeout;

		return $this;
	}
	//產生新的SESSION
	public function make($userid,$data=array(),$extra_timeout=0){
		/*
			aid:
			userid:
			data:{
				user:
				lmt:
				data:
			}
			extra_timeout:
		*/
		$userid=$userid;
		$aid=$data['aid'];
		$hash=$this->hash_make($userid);
		$user=$data['user'];
		$lmt=$data['lmt'];
		$data=$data['data'];

		$to_data=array(
				'aid'=>$aid,
				'userid'=>$userid,
				'user'=>$user,
				'hash'=>$hash,
				'lmt'=>$lmt,
				'data'=>$data,
				'extra_timeout'=>$extra_timeout
			);
		$this->insert($to_data);

		$data=$this->session_data;

		return $data;
	}
	//讀取SESSION
	public function load($userid,$hash){
		/*
			userid:
			hash:
		*/
		$userid=strtolower($userid);
		$to_data=array(
				'userid'=>$userid,
				'hash'=>$hash
			);

		$this->select($to_data);

		$re=$this->session_status;
		if($re){
			$gdata=$this->session_data;
			$this->upd_cookie('','',$gdata['extra_timeout']);
		}else{
			$gdata=$re;
		}
		

		return $gdata;
	}
	//刪除SESSION
	public function delete($userid,$hash){
		/*
			userid:
			hash:
		*/
		$userid=strtolower($userid);
		$to_data=array(
			'userid'=>$userid,
			'hash'=>$hash
		);
		$this->del_data($to_data);
		$this->flush();

		$this->del_cookie($userid,$hash);

		$re=$this->session_status;

		return $re;
	}
	/*
		###PRIVATE###
	*/
	private function insert($data=array()){
		/*
			userid:
			aid:
			user:
			hash:
			lmt:
			data:
			extra_timeout:
		*/
		$aid=$data['aid'];
		$userid=$data['userid'];
		$user=$data['user'];
		$hash=$data['hash'];
		$lmt=$data['lmt'];
		$extra_timeout=$data['extra_timeout'];

		$aid=(is_numeric($aid)?$aid:0);
		$lmt=(is_numeric($lmt)?$lmt:0);

		$data=json_encode($data['data']);
		$ins_str="INSERT INTO `session` (`aid`,`userid`,`user`,`hash`,`lmt`,`data`,`extra_timeout`,`timestamp`) 
			VALUES (
				'".$aid."','".$userid."','".$user."','".$hash."','".$lmt."','".$data."','".$extra_timeout."','".time()."'
			);";

		$re=$this->db->query($ins_str);

		$to_data=array(
				'aid'=>$aid,
				'userid'=>$userid,
				'user'=>$user,
				'hash'=>$hash,
				'lmt'=>$lmt
			);

		$this->upd_cookie($hash,$userid,$extra_timeout);

		$this->session_status=$re;
		$this->session_data=$to_data;

		return $this;
	}
	private function select($data=array()){
		/*
			userid:
			hash:
		*/
		$userid=strtolower($data['userid']);
		$hash=$data['hash'];

		$dead_time=time()-$this->timeout;

		$sel_str="SELECT * FROM `session` 
				WHERE `userid`='".$userid."' AND `hash`='".$hash."' AND `timestamp`>'".$dead_time."';";

		$re=$this->db->query($sel_str);
		$session_data=$this->db->fetch_assoc();
		$session_count=$this->db->num_rows();

		//if no data then false
		if($session_count<1){
			$re=false;
		}

		//if less time then skip update
		if($re && ($session_data['timestamp']+$this->set_update_time)<time()){
			$this->update($data);
		}

		$this->session_status=$re;
		$this->session_data=$session_data;

		return $this;
	}
	private function del_data($data=array()){
		/*
			userid:
			hash:
		*/
		$userid=$data['userid'];
		$hash=$data['hash'];
		$del_str="DELETE FROM `session` WHERE `hash`='".$hash."';";

		$re=$this->db->query($del_str);

		$this->session_status=$re;
		$this->session_data='';

		return $this;
	}
	private function update($data=array()){
		/*
			userid:
			hash:
		*/
		$userid=$data['userid'];
		$hash=$data['hash'];
		$upd_str="UPDATE `session` 
				SET `timestamp`='".time()."'+`extra_timeout` 
				WHERE `hash`='".$hash."';";

		$re=$this->db->query($upd_str);

		$this->session_status=$re;
		$this->session_data='';
		return $this;
	}
	private function flush(){
		if(time()%10!=0){//if not in half minute time
			return $this;
		}
		$timeout=$this->timeout;

		$dead_time=time()-$timeout;

		$del_str="DELETE LOW_PRIORITY FROM `session` 
				WHERE `timestamp`<".$dead_time.";";

		$this->db->query($del_str);
	
		return $this;
	}
	private function upd_cookie($hash='',$userid='',$extra_timeout=0){//更新COOKIE timeout
		$cookie_timeout=$this->timeout;
		
		$cookie_login_hash=\ANK::$input->cookie('login_hash');
		$cookie_login_userid=\ANK::$input->cookie('login_userid');
		
		$hash=(strlen($hash)<1?$cookie_login_hash:$hash);
		$userid=(strlen($userid)<1?$cookie_login_userid:$userid);

		$cookie_expired_time=(time()+$cookie_timeout+$extra_timeout);
			
		\ANK::$input->set_cookie("login_hash",$hash,$cookie_expired_time,'/');
		\ANK::$input->set_cookie("login_userid",$userid,$cookie_expired_time,'/');	
	}
	private function del_cookie($hash='',$userid=''){//刪除COOKIE
		\ANK::$input->set_cookie("login_hash",$hash,0,'/');
		\ANK::$input->set_cookie("login_userid",$userid,0,'/');
	}
	private function hash_make($userid){
		$hash_str=$userid.time();
		$hash=sha1($hash_str);

		return $hash;
	}
}
?>