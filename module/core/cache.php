<?php
/*
	CORE CACHE
*/
class CACHE{
	public function __construct(){
		$this->page = new CACHE_PAGE;
		$this->part = new CACHE_PART;
		$this->db = new CACHE_DB;
	}
}

class CACHE_PAGE{
	public function __construct(){
		$this->make_status=false;
	}
	public function make($url,$expire=600){
		$this->make_status=true;
		$this->make_url=$url;
		$this->make_expire=$expire;

		return $this;
	}
	//delete cache page
	public function delete($url){
		$url_path=$this->itn_make_filename($url);	
		$re=$this->itn_del_file($url_path);

		return $re;
	}
	//update caching file
	public function update($url,$expire=600){
		/*

		*/
		$this->delete($url);
		$this->make($url,$expire);

		return $this;
	}
	//check this caching file
	public function load($flush=false){
		/*
			flush:false if true then delete cache file
		*/
		//### if flush cache start
		if($flush){
			$url_get_arr=ANK::$input->get();
			//remove ANK data
			unset($url_get_arr['ank_handle'],$url_get_arr['ank_others']);
			$get_cnt=count($url_get_arr);

			$url_route_arr=ANK::$route->get('basic');
			$url_route='/'.implode('/',$url_route_arr);
			
			if($get_cnt>1){//if over 1 value
				unset($url_get_arr['cache_flush']);
				$url_tmp_arr=[];
				foreach ($url_get_arr as $key => $val) {
				    $url_tmp_arr[]=$key.'='.$val;
				}
				$file_url=$url_route.'?'.implode('&',$url_tmp_arr);
			}else{//if only cache_flush value
				$file_url=$url_route;
			}

			//DELETE cache file
			$this->itn_del_file($file_url);

			return false;
		}else{$file_url='';}
		//### if flush cache end

		$file_path = $this->itn_make_filename($file_url);

		//判斷是否過期
		if($this->itn_expired($file_path)){
			$html=$this->get_cache_data();
		}else{//重新產生
			return false;
		}

		return $html;
	}
	public function make_cache_data($html=''){
		$file_path = $this->itn_make_filename();
		$url_path=ANK::$route->get('path');

		$expire=$this->make_expire;
		$url=$this->make_url;

		if($url==$url_path){
			//cache header
			$file_content='<!--'.$this->itn_make_timestamp().'&'.$expire."-->\n".$html;

			file_put_contents($file_path, $file_content);

			$re=true;
		}else{
			$re=false;
		}

		return $re;
	}
	public function get_cache_data(){
		$file_path = $this->itn_make_filename();

		$html=file_get_contents($file_path);

		return $html;
	}
/*
	internal function
*/
	public function itn_make_timestamp(){
		return time();
	}
	public function itn_make_filename($url_path=''){
		$cache_path = ROOT_PATH.'module/cache/';
		if($url_path==''){
			$url_path=ANK::$route->get('path');
		}

		$filename = substr(sha1($url_path),0,15).'.cache';

		$file_path = $cache_path.$filename;

		return $file_path;
	}
	//checck if expired
	public function itn_expired($file_path){
		if(file_exists($file_path)){
			$html=file_get_contents($file_path);
			$sign_pos=strpos($html,"-->\n");
			$r1_html=substr($html,0,$sign_pos);
			$r2_data=str_replace('<!--','',$r1_html);
			$rf=explode('&',$r2_data);
			$cal_time=(intval($rf[0])+intval($rf[1]));

			if($cal_time>time()){//on time
				$re = true;
			}else{//expired
				$re = false;
			}
		}else{
			$re=false;
		}

		return $re;
	}
	//delete cache file
	public function itn_del_file($file_path){
		//$file_path=$this->itn_make_filename($file_url);
		$re=unlink($file_path);
		

		return $re;
	}
}
/*
	CACHE PART
*/
class CACHE_PART extends CACHE_SHARE{
	public function __construct(){
		$this->make_status=false;
		$this->make_type='part';//set this cache type
	}
	public function make($name,$data,$expire=600){
		$this->make_status=true;
		$this->make_name=$name;
		$this->make_data=$data;
		$this->make_expire=$expire;

		$re=$this->itn_make_file();
		if($re===false){
			\ANK_LOG::stack('failed to create cache file','cache');
		}

		return $this;
	}
	public function load($name){
		//get file data
		$data=$this->get_cache_data($name);
			
			//if no data cache
		if($data===false){
			return false;
		}
		//if not expired
		$ontime=$this->is_ontime($data);
			//if ontime then return data
		if($ontime===true){
			//revocery data
			$sign_pos=strpos($data,"\n");
			$r1_data=substr($data,($sign_pos+1));
			$re_data=json_decode(base64_decode($r1_data));
			
			return $re_data;
		}
			//if expired then return false
		return false;
	}
	public function delete($name){
		$file_path=$this->itn_get_filepath($name);	
		$re=$this->itn_del_file($file_path);

		return $re;
	}
}
/*
	CACHE DB
*/
class CACHE_DB extends CACHE_SHARE{
	public function __construct(){
		$this->make_status=false;
		$this->make_type = 'db';
	}
	//make($data,$query,$expire)
	public function make($query,$data,$expire=600){
		/*
			$data:[for array]
			$query:"select * from ...."(string)
			$expire:over 30 (sec)
		*/
		$this->make_status=true;

		$query_str=$this->itn_make_query_hash($query);
		$this->make_name=$query_str;

		$this->make_data=$data;
		$this->make_expire=$expire;

		$re=$this->itn_make_file();
	}
	//load($query)
	public function load($query){
		//process query string
		$name=$this->itn_make_query_hash($query);

		//get file data
		$data=$this->get_cache_data($name);
			
			//if no data cache
		if($data===false){
			return false;
		}
		//if not expired
		$ontime=$this->is_ontime($data);
			//if ontime then return data
		if($ontime===true){
			//revocery data
			$sign_pos=strpos($data,"\n");
			$r1_data=substr($data,($sign_pos+1));
			$re_data=json_decode(base64_decode($r1_data),true);
			
			return $re_data;
		}
			//if expired then return false
		return false;
	}
	//delete($query)
	public function delete($query){
		//process query string
		$name=$this->itn_make_query_hash($query);
		
		$file_path=$this->itn_get_filepath($name);	
		$re=$this->itn_del_file($file_path);

		return $re;
	}
/*
	internal function 
*/
	//process query string
	private function itn_make_query_hash($query){
		$re = trim($query);

		return $re;
	}
}

abstract class CACHE_SHARE{
/*
	SHARE global function
*/

/*
	SHARE internal function
*/
	//delete ile
	public function itn_del_file($path){
		unlink($path);
	}
	public function itn_make_file(){
		$name=$this->make_name;
		$data=$this->make_data;

		$file_path=$this->itn_get_filepath($name);
		$now=time();
		$expire_time=$this->make_expire;
		

		$header="{$now}&{$expire_time}\n";
		$en_data=base64_encode(json_encode($data,true));

		$file_content=$header.$en_data;

		$re=file_put_contents($file_path, $file_content);

		return $re;
	}
	public function is_ontime($data){
		$sign_pos=strpos($data,"\n");
		$r1_data=substr($data,0,$sign_pos);

		$rf=explode('&',$r1_data);
		$cal_time=(intval($rf[0])+intval($rf[1]));

		if($cal_time>time()){//on time
			return true;
		}

		//expired
		return false;
	}
	public function get_cache_data($name){
		$file_path = $this->itn_get_filepath($name);
		
		//if data is exist
		if(file_exists($file_path)){
			$data=file_get_contents($file_path);

			return $data;
		}

		return false;
	}
	//get file_path 
	public function itn_get_filepath($name){
		$cache_type = $this->make_type;//get cache type
		$cache_path = ROOT_PATH."module/cache/{$cache_type}/";
		$filename = substr(sha1($name),3,18).'.cache';

		$file_path = $cache_path.$filename;

		return $file_path;
	}
}
?>