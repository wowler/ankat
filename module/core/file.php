<?php
/*
	ANK core file
	(windows file system is not applicable)
*/
class FILE{
	public function __construct(){

	}
	//ANK put file
	public function put($site,$content,$type='DEFAULT'){
		//get site
		$site=$this->make_site($site);
		//switch which type
		switch($type){
			case 'JSON':
				$final_content=json_encode($content,JSON_UNESCAPED_UNICODE);
			break;
			case 'DEFAULT':
			default:
				$final_content=$content;
			break;
		}

		if(empty($site))return false;

		$re=file_put_contents($site,$final_content);
	
		return $re;
	}
	//prepend file (only text format)
	public function prepend($site,$content){
		//get site
		$site=$this->make_site($site);

		//check if data is exist
		if(file_exists($site)){
			$final_content=file_get_contents($site);
		}

		$final_content=$content.$final_content;

		$re=file_put_contents($site,$final_content);

		return $re;
	}
	//prepend file (only text format)
	public function append($site,$content){
		//get site
		$site=$this->make_site($site);

		//check if data is exist
		if(file_exists($site)){
			$final_content=file_get_contents($site);
		}

		$final_content=$final_content.$content;

		$re=file_put_contents($site,$final_content);

		return $re;
	}
	//get file contents
	public function get($site,$type='DEFAULT'){
		//get site
		$site=$this->make_site($site);
		
		switch($type){
			case 'LINE':
				$final_content=file($site);
			break;
			case 'JSON':
				$final_content=file_get_contents($site);
				$final_content=json_decode($final_content,true);
				if(empty($final_content)){
					\ANK_LOG::stack('parser JSON ERROR or no file content','fdb');
				}
			break;
			case 'DEFAULT':
			default:
				$final_content=file_get_contents($site);
			break;
		}	

		return $final_content;
	}
	//delete file
	public function delete($site){
		//check if is array
		if(is_array($site)){
			foreach($site as $inx => $val){
				$tmp_site=$this->make_site($val);
				$re=unlink($tmp_site);
			}
		}

		//if is string
		if(is_string($site)){
			$tmp_site=$this->make_site($site);
			$re=unlink($tmp_site);
		}

		return $re;
	}
	//move file
	public function move($source_site,$target_site,$type='DEFAULT'){
		$type=strtoupper($type);
		$source_site=$this->make_site($source_site);
		$target_site=$this->make_site($target_site);

		//if target file exist 
		if($type=='DEFAULT' && file_exists($target_site)){
			//nothing to do
			return false;
		}

		//if is force to replace file
		if($type=='FORCED'){
			//continue
		}

		$re=rename($source_site,$target_site);

		return $re;
	}
	//copy file
	public function copy($source_site,$target_site,$type='DEFAULT'){
		$type=strtoupper($type);
		$source_site=$this->make_site($source_site);
		$target_site=$this->make_site($target_site);

		//if target file exist 
		if($type=='DEFAULT' && file_exists($target_site)){
			//nothing to do
			return false;
		}

		//if is force to replace file
		if($type=='FORCED'){
			//continue
		}

		$re=copy($source_site,$target_site);

		return $re;
	}
	//LIST files and folder
	public function listDIR($site='',$get_type='ALL'){
		/*
			will list files or folder in folder

			get_type: ALL | FOLDER | FILE | LINK
		*/
		$tmp_site=$this->make_site($site);

		$get_type=strtoupper($get_type);

		$rd=[];

		foreach (new DirectoryIterator($tmp_site) as $fileInfo){
		    if($fileInfo->isDot()) continue;
		    $name=$fileInfo->getFilename();
		    $type=$fileInfo->getType();
		    $size=$fileInfo->getSize();
		    $ext=$fileInfo->getExtension();

		    if($get_type=='FOLDER' && $type!='dir')continue;
		    if($get_type=='FILE' && $type!='file')continue;
		    if($get_type=='LINK' && $type!='link')continue;

		    $rd[]=[
		    	'name'=>$name,
		    	'ext'=>$ext,
		    	'type'=>$type,
		    	'size'=>$size
		    ];
		}

		return $rd;
	}
/*
	make directory
*/
	public function makeDIR($site){
		$tmp_site=$this->make_site($site);

		$re=mkdir($tmp_site);

		return $re;
	}
	public function deleteDIR($site){
		$tmp_site=$this->make_site($site);
	
		$re=rmdir($tmp_site);

		return $re;
	}
/*
	others function
*/
	//check file or directory exist or not
	public function file_exists($site){
		$tmp_site=$this->make_site($site);

		$re=file_exists($tmp_site);

		return $re;
	}
	//get size of file
	public function size($site,$stype='DEFAULT'){
		$tmp_site=$this->make_site($site);

		//check if is exist
		if(!file_exists($tmp_site)){
			return false;
		}
		$size=filesize($tmp_site);

		$stype=strtoupper($stype);

		switch($stype){
			case 'GB':
				$re=number_format($size/1073741824,2);
			break;
			case 'MB':
				$re=number_format($size/1048576,2);
			break;
			case 'KB':
				$re=number_format($size/1024,2);
			break;
			case 'B':
				$re=$size;
			break;
			case 'AUTO':
			default:
				$re=$this->auto_size($size);
			break;
		}

		return $re;
	}
/*
	internal function
*/
	private function make_site($site){
		/*
			{{ROOT_PATH}}:replace root_paht
			{{EXT_PATH}}:replace ext_path
		*/
		//check if relatively site or absolute site
		$re_site=$site;

		//replace if use magic site
		$magic_replace=false;
		if(strpos($site,'{{ROOT_PATH}}')!==false){
			$t=substr(ROOT_PATH,0,-1);
			$re_site=str_replace('{{ROOT_PATH}}',$t,$site);
			$magic_replace=true;
		}

		
		if(strpos($site,'{{EXT_PATH}}')!==false){
			$t=substr(EXT_PATH,0,-1);
			$re_site=str_replace('{{EXT_PATH}}',$t,$site);
			$magic_replace=true;
		}

		$os=\ANK_CONF::get('os');

		if($os=='WINDOWS'){
			$key_sign=substr($site,1,2);
			if($magic_replace==false){
				if($key_sign==':\\'){//is absolute site(windows)
					$re_site=$site;
				}else{//set relatively site of APP root
					$re_site=ROOT_PATH.$site;
				}
			}

			$re_site=realpath($re_site);
		}

		if($os==='LINUX'){
			$first_sign=substr($site,0,1);

			if($magic_replace==false){
				if($first_sign=='/'){//is absolute site(Linux)
					$re_site=$site;
				}else{//set relatively site of APP root
					$re_site=ROOT_PATH.$site;
				}
			}
		}

		return $re_site;
	}
	//auto cal file size and unit
	private function auto_size($size){
		if($size>=1073741824){
			return number_format($size/1073741824,2).' GB';
		}

		if($size>=1048576){
			return number_format($size/1048576,2).' MB';
		}

		if($size>=1024){
			return number_format($size/1024,2).' KB';
		}

		if($size>1){
			return $size.' bytes';
		}

		if($size==1){
			return '1 byte';
		}

		return '0';
	}
}
?>