<?php
/*
	CORE INPUT ver 0.8a
	by jimpop 2015/09/30 15:04
*/
//about others input data
class INPUT{
	var $p_d,$g_d,$c_d,$s_d;
	public function __construct(){	
		$this->p_d=$_POST;
		$this->g_d=$_GET;
		$this->c_d=$_COOKIE;
		$this->s_d=$_SERVER;
		$this->ps_d=null;

		$this->security=new SECURITY;

		return $this;
	}
	public function post($name='',$sec=true){
		if(strlen($name)<1){
			$res=$this->p_d;
		}else{
			$res=$this->get_val($name,'post');
		}

		if($sec){
			$res=$this->security->basic_clean($res);
		}

		return $res;
	}	
	public function get($name='',$sec=true){
		if(strlen($name)<1){
			$res=$this->g_d;
		}else{
			$res=$this->get_val($name,'get');
		}

		if($sec){
			$res=$this->security->basic_clean($res);
		}

		return $res;
	}	
	public function cookie($name='',$sec=true){
		if(strlen($name)<1){
			$res=$this->c_d;
		}else{
			$res=$this->get_val($name,'cookie');
		}

		if($sec){
			$res=$this->security->basic_clean($res);
		}

		return $res;
	}
	public function server($name=''){//get send method
		if(strlen($name)<1){
			$res=$this->s_d;
		}else{
			$res=$this->get_val($name,'server');
		}

		return $res;
	}
	public function session($name=''){//get send method
		//if no set loacl variable
		if($this->ps_d==null)$this->ps_d=$_SESSION;

		if(strlen($name)<1){
			$res=$this->ps_d;
		}else{
			$res=$this->get_val($name,'session');
		}

		return $res;
	}
	//get php input area
	public function phpinput($decode_type='raw'){
		$re = file_get_contents('php://input');

		switch($decode_type){
			case 'json':
				$re=json_decode($re,true);
			break;
			default:
			break;
		}

		return $re;
	}
	//for destroying PHP SESSION 
	public function destory_session(){
		$this->ps_d=null;
		session_destroy();
	}
	public function list_all($type){//list all the array
		switch($type){
			case 'get':
				$res=$this->g_d;
			break;
			case 'post':
				$res=$this->p_d;
			break;
			case 'server':
				$res=$this->s_d;
			break;
			case 'cookie':
				$res=$this->c_d;
			break;
			case 'session':
				$res=$this->ps_d;
			break;
		}

		return $res;
	}
	public function set($type,$name,$val){
		$type=strtolower($type);
		switch($type){
			case 'get':
				$this->g_d[$name]=$val;
			break;			
			case 'post':
				$this->p_d[$name]=$val;
			break;
			case 'session':
				$this->ps_d[$name]=$val;
				$_SESSION[$name]=$val;
			break;
		}

		return $this;
	}
	/*
set function
	*/
	public function set_cookie($name,$value,$timeout=3600,$path=null,$domain=null){
		/*
			name:
			value:
			timeout: default is 3600 , if delete set -1
		*/
		setcookie($name,$value,time()+$timeout,$path,$domain);
	}
	/*
internal function
	*/
	private function get_val($name,$type){
		switch($type){
			case 'get':
				$res=$this->g_d[$name];
			break;			
			case 'post':
				$res=$this->p_d[$name];
			break;			
			case 'cookie':
				$res=$this->c_d[$name];
			break;			
			case 'server':
				$res=$this->s_d[$name];
			break;
			case 'session':
				$res=$this->ps_d[$name];
			break;

		}

		if(gettype($res)=="NULL")$res=false;

		return $res;
	}
}
?>