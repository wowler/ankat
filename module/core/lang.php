<?php
/*
	core for language
*/
class LANG{
	public $lang_charset,$lang_pos;
	public function __construct($lang_charset='en'){
		//default language charset
		$this->set_charset($lang_charset);

		$this->lang_datas=[];//storage include language file
	}
	//set language charset
	public function set_charset($lang_charset='en'){
		$this->lang_charset=$lang_charset;

		return $this;
	}
	//load & set language file
	public function load($file_name,$md='|na|',$user='|na|'){
		//prepare filename for reducing path length
		$basic_site="lang/{$this->lang_charset}/";
		$site=$basic_site.$file_name;
		
		$path=\ANK::git($site,$md,$user);
		$filename_encode=str_replace(EXT_PATH,'',$path);

		//set position for now
		$this->lang_pos=$filename_encode;

		//for cache data
		if(!is_null($this->lang_datas[$this->lang_charset][$filename_encode])){
			return $this->lang_datas[$this->lang_charset][$filename_encode];
		}

		$rd=require $path;

		//storage language file to array object
		$this->lang_datas[$this->lang_charset][$filename_encode]=$rd;

		return $rd;
	}
	//get now language charset
	public function now_lang(){
		return $this->lang_charset;
	}
}

//for global function to get location string
function _L(){
	//get needed argus
	$args = func_get_args();

	//load now language resource(from position)
	$charset=\ANK::$lang->lang_charset;
	$pos=\ANK::$lang->lang_pos;
	
	$rd=\ANK::$lang->lang_datas[$charset][$pos];

	$td=$rd;
	foreach($args as $inx => $val){
		$td=$td[$val];
	}

	echo $td;
}
?>