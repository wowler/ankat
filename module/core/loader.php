<?php
/*
	CORE LOADER
*/
class LOADER{
	public $ready_include,$logs;
	public $has_loader;
	public function __construct(){
		$this->ready_include=array();
		$this->has_loader=[];

	}
	public function set_loader($input){
		$ready_include=$this->ready_include;
		if(gettype($input)=='array'){//if input is array then merge
			$this->ready_include=array_merge($ready_include,$input);
		}else{
			$this->ready_include[]=$input;
		}

		return $this;
	}
	public function autoload($arr=array()){
		/*
			arr:if direct load in array
		*/
		//IF input String,merge to an array
		if(is_string($arr)){
			$arr=[$arr];
		}
		
		if(count($arr)>0){//if arr is set
			$ready_include=$arr;
		}else{
			$ready_include=$this->ready_include;
		}	

		$log_success=0;
		$log_error=0;

		for($i=0,$cnt=count($ready_include);$i<$cnt;$i++){
			$site=$ready_include[$i];
		
			if(file_exists($site)){
				if($this->itn_chk_if_loaded($site)){
					include $site;
					$this->itn_stack_new_site($site);
					$log_success++;
				}else{//had loaded

				}
			}else{
				$log_error++;
			}
			
		}

		$this->logs=array(
			'success'=>$log_success,
			'error'=>$log_error
		);

		return $this;
	}
	public function result(){//get result
		return $this->logs;
	}
	public function clear(){//clear all cache
		$this->ready_include=array();
		
		return $this;
	}
	/*
internal function
	*/
	private function itn_chk_if_loaded($site){
		$has_loader=$this->has_loader;
		if(in_array($site,$has_loader)){
			$re=false;
		}else{
			$re=true;
		}

		return $re;
	}
	private function itn_stack_new_site($site){
		$has_loader=$this->has_loader;
		if($this->itn_chk_if_loaded($site)){
			$has_loader[]=$site;
			$this->has_loader=$has_loader;
		}



		return $this;
	}
}
?>