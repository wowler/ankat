<?php
/*
	file DB
*/
class FDB{
	public function __construct(){
		
	}
	public function make($DBname){
		return new FDB_CLASS($DBname);
	}
}
class FDB_CLASS{
	public function __construct($DBname,$user='USER'){
		//setting this FDB DBname
		$this->DBname=$DBname;

		//who use this FDB
		$this->user=$user;

		//init wait lock time
		$this->wait_lock_time=0;

		//init output type(default is normal)
		$this->output_type='normal';
	}
/*
	USER SET
*/
	//set output data format
	public function set_output($type='normal'){
		$this->output_type=$type;

		return $this;
	}
	//
	public function get($tb_name,$type,$limit_start=0,$limit_length=10){
		/*
			type:"serial":
		*/
		$path=$this->get_path($tb_name);

		//get which DB type
		$fdb_setting=$this->get_config($tb_name);

		switch ($fdb_setting['type']){
			case 'serial':
				$file_path=$path."{$type}.fdb";
				//if file is not exist
				if(!file_exists($file_path)){
					return false;
				}

				$re=\ANK::$file->get($file_path,'JSON');
			break;
			case 'key':
				$file_path=$path."{$type}.fdb";
				//if file is not exist
				if(!file_exists($file_path)){
					return false;
				}

				$re=\ANK::$file->get($file_path,'JSON');
			break;
			default:
				
			break;
		}

		return $re;
	}
	//
	public function put($tb_name,$key,$data){
		/*
			type:"serial":
		*/
		$path=$this->get_path($tb_name);

		//get which DB type
		$fdb_setting=$this->get_config($tb_name);

		switch ($fdb_setting['type']){
			case 'serial':
				//if is update data
				if(!empty($key)){
					$file_path="{$path}{$key}.fdb";
					$re=\ANK::$file->put($file_path,$data,'JSON');

					break;
				}
				
				$re=$this->set_config($tb_name);
				if($re==true){
					$next=$this->next_auto_increment_key;
					$file_path="{$path}{$next}.fdb";
					$re=\ANK::$file->put($file_path,$data,'JSON');
				}
			break;
			case 'key':
				$file_path=$path."{$key}.fdb";

				//if file is exist
				if(file_exists($file_path)){
					return false;
				}

				$re=\ANK::$file->put($file_path,$data,'JSON');
			break;
			default:
				
			break;
		}

		return $re;
	}
	//
	public function update($tb_name,$key,$data){
		/*
			type:"serial":
		*/
		$path=$this->get_path($tb_name);

		$file_path=$path."{$key}.fdb";

		//lock file
		$re=$this->lock_data($file_path);
		if($re===false)return false;

		//if file is not exist
		if(!file_exists($file_path)){
			return false;
		}

		$re=\ANK::$file->put($file_path,$data,'JSON');

		//unlock file
		$re=$this->unlock_data($file_path);

		if($re===false)return false;

		return $re;
	}
	//
	public function delete($tb_name,$key){
		$path=$this->get_path($tb_name);

		$file_path=$path."{$key}.fdb";

		//if file is not exist
		if(!file_exists($file_path)){
			return false;
		}

		$re=\ANK::$file->delete($file_path);

		return $re;
	}
	//select list
	public function select($tb_name,$orderby='ASC',$limit_start=0,$limit_length=20){
		/*
				
		*/
		$path=$this->get_path($tb_name);

		$list_datas=\ANK::$file->listDIR($path);

		//sort by key

		$arrdb=\ANK::$arrDB->make($list_datas);
		$list_datas=$arrdb->orderby(['name'=>$orderby])->get();

		$tmp_rd=[];
		//filter no need ext
		$cnt=-1;
		$stop_pos=$limit_start+$limit_length;
		foreach($list_datas as $inx => $val){
			if($val['ext']!='fdb'){
				continue;
			}
			$cnt++;
			//if in range
			if($limit_start <= $cnt && $cnt<=$stop_pos){
				//if over stop position
				if($cnt>=$stop_pos){
					break;
				}
				$tmp_rd[]=$val;
			}
		}

		$rd=[];
		//load every data
			//output format
			$output_setting=$this->output_type;
		foreach($tmp_rd as $inx => $val){
			$tmp_name=str_replace('.fdb','',$val['name']);

				//if output type is normal
			if($output_setting=='normal'){
				$td=$this->get($tb_name,$tmp_name);
				$td['_key']=$tmp_name;
				$rd[]=$td;

				continue;
			}

			$rd[]=[
				'key'=>$tmp_name,
				'data'=>$this->get($tb_name,$tmp_name)
			];
		}

		return $rd;
	}
/*
	internal function
*/
	public function set_config($tb_name,$type='serial',$method='add_ai'){
		//basic init
		$type=strtolower($type);
		$method=strtolower($method);

		//base path
		$path=$this->get_path($tb_name);

		//lock file
		$file_path="{$path}_fdb.cfg";
		$re=$this->lock_data($file_path);
		if($re===false)return false;
		
		//start set config
			//load config
		$fdb_config_path="{$path}_fdb.cfg";
		$tb_config=\ANK::$file->get($fdb_config_path,'JSON');


		switch($type){
			case 'serial':
				if($method=='add_ai'){
					$ai=intval($tb_config['auto_increment'])+1;
					$tb_config['auto_increment']=$ai;
					$this->next_auto_increment_key=$ai;
				}
			break;
		}

		//save new config
		$re=\ANK::$file->put($fdb_config_path,$tb_config,'JSON');

		if($re===false){
			return false;
		}

		//unlock file
		$re=$this->unlock_data($fdb_config_path);

		if($re===false){
			return false;
		}

		return true;
	}
	public function get_config($tb_name){
		$path=$this->get_path($tb_name);
		$path.="_fdb.cfg";

		//GET CONFIG
			//check if file is exist
			if(!file_exists($path)){
				return false;
			}
			//else get config setting
			$re=\ANK::json_load($path);
	
		return $re;
	}
	//get file path
	public function get_path($tb_name){
		//
		$db_name=$this->DBname;

		$tmp_path=ROOT_PATH."module/fdb/{$db_name}/{$tb_name}/";

		return $tmp_path;
	}
/*
	data lock
*/
	public function lock_data($file_path){
		/*
			
		*/
		//make a lock file
			$file_lock_path="{$file_path}.lock";
		//if cannot lock file
		$chk_fun=function($file_lock_path){
			if(file_exists($file_lock_path)){
				return false;
			}
			return true;
		};

		//add data to lock file
		$ins_fun=function($data,$count=0) use (&$ins_fun){
			//if try count over 3 then false
			if($count>3)return false;
			$file_lock_path=$data['file_lock_path'];
			$re=\ANK::$file->put($file_lock_path,[
				'time'=>time(),
				'user'=>$data['user']
			],'JSON');

			//if create lock file fail try again
			if($re===false){
				$rand_number=rand(100000,300000);//for 100ms to 300ms random
				usleep($rand_number);
				$count+=1;
				return $ins_fun($data,$count);
			}

			return $re;
		};

		$wait_lock_time=0;
		while($wait_lock_time<3000000){
			$rand_number=rand(100000,300000);//for 100ms to 300ms random
			$wait_lock_time+=$rand_number;//set this waitting time
			usleep($rand_number);
			$re=$chk_fun($file_lock_path);
			//if success
			if($re){
				//insert lock info data
				$re=$ins_fun([
					'file_lock_path'=>$file_lock_path,
					'user'=>$this->user
				]);
				return $re;
			}
		}

		return false;
	}
	public function unlock_data($file_path){
		//make a lock file
		$file_lock_path="{$file_path}.lock";
		//unlock file
		$re=\ANK::$file->delete($file_lock_path);

		return $re;
	}
}
?>