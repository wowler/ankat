<?php
/*
	core AJAX ver 0.9a
	by Jimpop 2016/03/24 10:26
*/
abstract class AJAX{
	public $ajax_send_type;
	public $ajax_send_data;
	public $ajax_init_status=false;
	public function ajax_init(){
		$this->ajax_send_data=array();

		//set default set type
		$this->ajax_set_type();

		$ajax_init_status=true;

		$this->ajax_debug();//default for debug true

		return $this;
	}
	public function ajax_set_type($ajax_send_type='json'){
		switch($ajax_send_type){
			case 'php':
				$this->ajax_send_type='php';
			break;
			case 'xml':
				$this->ajax_send_type='xml';
			break;
			default:
				$this->ajax_send_type='json';
			break;
		}

		return $this;
	}
	public function ajax_set_data($name,$data){
		/*
			name:
			data:
		*/
		$this->ajax_send_data[$name]=$data;

		return $this;
	}
	public function put($val1=array(),$val2=array()){
		/*
			val1:[type | data array]
			val2:[data array]
		*/
		if(!$this->ajax_init_status){
			$this->ajax_init();
		}

		if(is_string($val1)){//if user set type first
			$type=$val1;
			$data=$val2;
		}else if(is_array($val1)){//if merge type into array
			$data=$val1;
			//if ajax_type is set , then use this first
			if(strlen($data['ajax_type'])>0){
				$type=$data['ajax_type'];
			}else{//then use type variable
				$type=$data['type'];
			}
		}
		
		//default call function
		if(method_exists($this,$type)){
			return $this->$type($data);
		}

		$sdata=[
			'status'=>false,
			'desc_str'=>'no ajax type set'
		];

		return $this->output($sdata);
	}
	public function ajax_debug($type='auto'){

		switch($type){
			case 'true':
			$this->ajax_debug_status=true;
			//if error_level is off then turn off
			if(\ANK_CONF::get('dev')['error_level']=='off'){
				$this->ajax_debug_status=false;
			}
			break;
			case 'false':
				$this->ajax_debug_status=false;
			break;
			case 'force':
				$this->ajax_debug_status=true;
			break;
			case 'auto':
			default:
				//auto set
				$this->ajax_debug_status=false;
				if(\ANK_CONF::get('dev')['status']=='true'){
					$this->ajax_debug_status=true;
				}
			break;
		}

		return $this;
	}
	public function output($data=null){
		/*
			data:
			name:
		*/
		$ajax_send_data=$this->ajax_send_data;
		$ajax_send_type=$this->ajax_send_type;

		//if data input use input data
		if(!is_null($data)){
			$ajax_send_data=$data;
		}

		//if debug status is true
		if($this->ajax_debug_status){
			$ajax_send_data['ank_debug']=\DEBUG::show();
			$ajax_send_data['ank_logs']=\ANK_LOG::show();
		}

		switch($ajax_send_type){
			case 'json':
				echo json_encode($ajax_send_data);
			break;
			case 'xml':
				
			break;
			case 'php':
			default:
				return $data;
			break;
		}
	}
}

?>