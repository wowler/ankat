<?php
/*
	CORE HTML
*/
class HTML{
	public function __construct($base=true){
		$this->base = $base;
		$this->make = new HTML_MAKE($this);
		$this->append = new HTML_APPEND($this);

		$this->uniqid=substr(uniqid(),-5,5);

		$this->output_html='';
		$this->label=array();
	}
	public function info($type='all'){
		$rd=[];

		switch($type){
			case 'all':
			default:
				$rd['label']=$this->label;
			break;
		}

		return $rd;
	}
	public function output($debug=false,$type='html'){
		/*
			type:
			debug:true | false
		*/
		if($type=='html'){
			$output_html=$this->output_html;
			if($debug==false){
				$replace_arr=[];
				for($i=0,$cnt=count($this->label);$i<$cnt;$i++){
					$t=$this->label[$i];
					$tarr=$this->make_label_result_arr($t);
					$replace_arr[]=$tarr['start'];
					$replace_arr[]=$tarr['end'];
				}
				$output_html=str_replace($replace_arr,'',$output_html);
			}
			$output_html=str_replace(['	',"\r\n"],'',$output_html);
			return $output_html;
		}
	}
	public function create(){
		return new HTML(false);
	}
	/*
		share function
	*/
	public function itn_make_label_tag($str=''){
		$str='<!--'.$str.'-->';

		return $str;
	}
	public function itn_get_label(){
		$rd=$this->label;
		
		return $rd;
	}
	//make label
	public function make_label($label_note){
		$inx=count($this->label);
		$uniqid=$this->uniqid;
		$label=$uniqid.'-'.$inx.'-'.$label_note;

		$rd=$this->make_label_result_arr($label);

		return $rd;
	}
	//make a array for start、end、normal
	public function make_label_result_arr($label){
		$lb_start=$label.'-start';
		$lb_end=$label.'-end';
		
		$rd=[
			'normal'=>$label,
			'start'=>$this->itn_make_label_tag($lb_start),
			'end'=>$this->itn_make_label_tag($lb_end)
		];

		return $rd;
	}
	//push label
	public function push_label($label,$type='normal'){
		/*
			type: normal | none
		*/
		if($this->base===true){//if its base then create new object
			$obj=$this->create();	
		}else{
			$obj=$this;
		}

		$obj->label[]=$label;

		if($type=='normal'){//if normal then set variable
			$obj->append->itn_set_label_target('','auto');//set last label to target
		}
		

		return $obj;
	}
	public function itn_make_attr_str($attr=array()){//產生屬性數值字串
		/*
			attr:
		*/
		$attr_arr=array();
		if(gettype($attr)=='array'){
			foreach($attr as $inx => $value){
				$attr_arr[]=$inx.'="'.$value.'"';
			}
			$attr_str=implode(' ',$attr_arr);
		}else{
			$attr_str='';
		}
		
	
		return $attr_str;
	}
	public function itn_make_prop_str($prop=array()){
		$prop_str='';

		$prop_arr=[];

		if(gettype($prop)=='array'){
			foreach($prop as $inx => $value){
				if($value==true){
					$prop_arr[]=$inx;
				}
			}
			$prop_str=implode(' ',$prop_arr);
		}else{
			$prop_str='';
		}

		return $prop_str;
	}
	//append html or set html
	public function itn_set_output_html($html){
		
		if(strlen($this->output_html)<1)$this->output_html=$html;
		else{
			$this->make->itn_proccess_html($html);
		}

		return $this;
	}
}

class HTML_MAKE extends HTML_SHARED{
	public function __construct($parent){
		$this->parent = $parent;
		$this->label_tag='na';//default for label note
		$this->label_user_tag_status=false;//if user set custom variable then set true
	}
	//table
	public function table($attr=[]){
		/*
			attr:
		*/
		$label_arr=$this->itn_set_label_note('table');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$attr_str=$obj->itn_make_attr_str($attr);//產生屬性字串

		$html=<<<HTML
		<table $attr_str>
			{$label_arr['start']}
			{$label_arr['end']}
		</table>
HTML;
		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//tr
	public function tr($attr=[]){
		/*
			attr:
		*/
		$label_arr=$this->itn_set_label_note('tr');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$attr_str=$obj->itn_make_attr_str($attr);//產生屬性字串

		$html=<<<HTML
		<tr $attr_str>
			{$label_arr['start']}
			{$label_arr['end']}
		</tr>
HTML;
		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//select
	public function select($attr=[]){
		/*

		*/
		$label_arr=$this->itn_set_label_note('select');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$attr_str=$obj->itn_make_attr_str($attr);//產生屬性字串
		
		$html=<<<HTML
		<select $attr_str>
			{$label_arr['start']}
			{$label_arr['end']}
		</select>
HTML;
		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//button
	public function button($attr=array(),$content=''){//BUTTON
		/*
		*/
		$label_arr=$this->itn_set_label_note('button');

		$obj=$this->parent->push_label($label_arr['normal']);

		$attr_str=$obj->itn_make_attr_str($attr);//產生屬性字串

		$html=<<<HTML
		<button $attr_str>{$label_arr['start']}{$content}{$label_arr['end']}</button>
HTML;

		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//input
	public function input($attr=array()){//BUTTON
		/*
		*/
		$label_arr=$this->itn_set_label_note('input');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$attr_str=$obj->itn_make_attr_str($attr);//產生屬性字串

		$html=<<<HTML
		<input $attr_str>{$label_arr['start']}{$label_arr['end']}</input>
HTML;

		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//div
	public function div($attr=array(),$content='',$prop=array()){
		$label_arr=$this->itn_set_label_note('div');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$html=$obj->make->itn_div('make',$attr,$content,$prop);

		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//html
	public function html($content=''){
		$label_arr=$this->itn_set_label_note('html');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$html=$obj->make->itn_html('make',$content);

		$obj->itn_set_output_html($html);

		return $obj->append;
	}
	//dom
	public function dom($dom_head='',$attr=array(),$content='',$prop=array()){
		$label_arr=$this->itn_set_label_note('dom');
		
		$obj=$this->parent->push_label($label_arr['normal']);

		$html=$obj->make->itn_dom('make',$dom_head,$attr,$content,$prop);

		$obj->itn_set_output_html($html);

		return $obj->append;
	}
/*
	internal function
*/
	//set label note 
	public function itn_set_label_note($label_default=''){
		/*
			label_default: (pritory less then user define)
		*/
		if($this->label_user_tag_status==true){
			$label_note=$this->label_tag;
			$label_arr=$this->parent->make_label($label_note);
		}else{//set label_default
			$label_arr=$this->parent->make_label($label_default);
		}

		$this->itn_default_label();//restore default setting

		return $label_arr;
	}
	
}

class HTML_APPEND extends HTML_SHARED{
	public function __construct($parent){
		$this->parent = $parent;
		$this->make = $parent->make;
		$this->label_target='';
		$this->label_tag='na';//if append function need tag
		$this->label_user_tag_status=false;//if user set custom variable then set true
	}
	public function option($content='',$attr=array(),$prop=array()){
		/*
			value:
			content:
			selected:true | false
			attr:[]
		*/
		//get label data array
		$label_arr=$this->itn_set_label_tag('option');

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<option $attr_str $prop_str>{$label_arr['start']}{$content}{$label_arr['end']}</option>
HTML;

		$this->itn_proccess_html($html);

		return $this;
	}
	public function thead($content='',$attr=[],$prop=[]){
		//get label data array
		$label_arr=$this->itn_set_label_tag('thead');

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<thead {$attr_str} {$prop_str}>
			{$label_arr['start']}
			{$content}
			{$label_arr['end']}
		</thead>
HTML;
		
		$this->itn_proccess_html($html);

		return $this;
	}
	public function tbody($content='',$attr=[],$prop=[]){

		//get label data array
		$label_arr=$this->itn_set_label_tag('tbody');

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<tbody {$attr_str} {$prop_str}>
			{$label_arr['start']}
			{$content}
			{$label_arr['end']}
		</tbody>
HTML;
		
		$this->itn_proccess_html($html);

		return $this;
	}
	//th
	public function th($content='',$attr=[],$prop=[]){

		
		//get label data array
		$label_arr=$this->itn_set_label_tag('th');

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<th {$attr_str} {$prop_str}>
			{$label_arr['start']}
			{$content}
			{$label_arr['end']}
		</th>
HTML;

		$this->itn_proccess_html($html);

		return $this;
	}
	//td
	public function td($content='',$attr=[],$prop=[]){

		//get label data array
		$label_arr=$this->itn_set_label_tag('td');

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<td {$attr_str} {$prop_str}>
			{$label_arr['start']}
			{$content}
			{$label_arr['end']}
		</td>
HTML;
		
		$this->itn_proccess_html($html);

		return $this;
	}
/*
	share function
*/
	//div
	public function div($attr=array(),$content='',$prop=array()){

		$html=$this->itn_div('append',$attr,$content,$prop);

		$this->itn_proccess_html($html);

		return $this;
	}
	//html
	public function html($content=''){

		$html=$this->itn_html('append',$content);

		$this->itn_proccess_html($html);

		return $this;
	}
	//dom
	public function dom($dom_head='',$attr=array(),$content='',$prop=array()){

		$html=$this->itn_dom('append',$dom_head,$attr,$content,$prop);

		$this->itn_proccess_html($html);

		return $this;
	}
	/*
internal function
	*/
	//set label tag for unit
	public function itn_set_label_tag($label_default=''){
		/*
			label_default: (pritory less then user define)
		*/
        $label_arr=['start'=>'','end'=>''];
        if($this->label_user_tag_status==true){
        	$label_note=$this->label_tag;
        	$label_arr=$this->parent->make_label($label_note);
        }else{
        	$label_arr=$this->parent->make_label($label_default);
        }

		$this->parent->push_label($label_arr['normal'],'none');
		$this->itn_default_label();


       return $label_arr;
	}
	public function output($debug=false,$type='html'){
		return $this->parent->output($debug,$type);
	}
}

/*
	HTML SHARE CLASS
*/
abstract class HTML_SHARED{
	//set set
	public function set($label=''){
		$this->label_tag=$label;
		$this->label_user_tag_status=true;

		return $this;
	}
	//default label 
	public function itn_default_label(){
		$this->label_tag='na';
		$this->label_user_tag_status=false;//resotre default label setting
	}
	public function to($label_str='',$method='find'){
		/*
			label_str:
			method:	find (default)
		*/
		$set_label_str='';

		switch($method){
			case 'find':
				$label_arr=$this->parent->label;
				$cnt=count($label_arr);
				for($i=0;$i<$cnt;$i++){
					$td=$label_arr[$i];
					if(strpos($td,$label_str)!==false){
						$set_label_str=$td;
					}
				}
			break;
			case 'absoulte':
			default:
				$set_label_str=$label_str;
			break;
		}

		$this->itn_set_label_target($set_label_str,'forced');

		return $this;
	}
	public function itn_set_label_target($label='',$type='auto'){
		/*
			type:forced | auto(default)
		*/
		$label_finally='';
		if($type=='forced'){//if forced set label
			$this->label_target=$label;

			return $this;
		}

		/*if(strlen($this->label_target)>0){//if has set,skip this proccess
			return $this;
		}*/
		if($type=='auto'){
			$label_arr=$this->parent->label;
			$label_finally=end($label_arr);

			$this->label_target=$label_finally;
		}
		
		return $this;
	}
	//proccess append unit html
	public function itn_proccess_html($html){
		$find_str=$this->parent->itn_make_label_tag($this->label_target.'-end');
		$source_html=$this->parent->output_html;

		$exp=explode($find_str,$source_html);

		$final_html=$exp[0].$html.$find_str.$exp[1];

		$this->parent->output_html=$final_html;

		return $this;
	}
/*
	DOM SHARE ITN
*/
	public function itn_div($sys_type='make',$attr=array(),$content='',$prop=array()){
		//push label array
		if($sys_type=='make'){
			$label_arr=$this->parent->make_label_result_arr($this->label_tag);
		}else{//append
			if(strlen($this->label_tag)>0){
				$label_arr=$this->parent->make_label($this->label_tag);
				$this->label_tag='';
			}
		}
		

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<div {$attr_str} {$prop_str}>{$label_arr['start']}{$content}{$label_arr['end']}</div>
HTML;

		return $html;
	}
	public function itn_html($sys_type='make',$content=''){
		//push label array
		if($sys_type=='make'){
			$label_arr=$this->parent->make_label_result_arr($this->label_tag);
		}else{//append
			if(strlen($this->label_tag)>0){
				$label_arr=$this->parent->make_label($this->label_tag);
				$this->label_tag='';
			}
		}

		$html=<<<HTML
		{$label_arr['start']}{$content}{$label_arr['end']}
HTML;

		return $html;
	}
	public function itn_dom($sys_type='make',$dom_head='',$attr=[],$content='',$prop=[]){
		//push label array
		if($sys_type=='make'){
			$label_arr=$this->parent->make_label_result_arr($this->label_tag);
		}else{//append
			if(strlen($this->label_tag)>0){
				$label_arr=$this->parent->make_label($this->label_tag);
				$this->label_tag='';
			}
		}

		$attr_str=$this->parent->itn_make_attr_str($attr);//產生屬性字串
		$prop_str=$this->parent->itn_make_prop_str($prop);//產生屬性字串

		$html=<<<HTML
		<{$dom_head} {$attr_str} {$prop_str}>{$label_arr['start']}{$content}{$label_arr['end']}</{$dom_head}>
HTML;

		return $html;
	}
}
?>