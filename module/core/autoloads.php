<?php
spl_autoload_register(function($class){
	
	$exp=explode('\\',$class);
	//echo json_encode($exp);exit;
	$cnt=count($exp);
	$user=$exp[0];
	$md_name=$exp[1];
	$type=$exp[$cnt-2].'/';
	$name=end($exp);

	if($cnt<4){//no type set
		$type='';
	}
	
	$root_site=EXT_PATH;

	$site=$root_site."{$user}/{$md_name}/md/{$type}{$name}.php";

	$site=strtolower($site);

	if(file_exists($site)){
		include $site;
		return ;
	}

	//fit in new version
	$site=$root_site."{$user}/{$md_name}/{$type}{$name}.php";

	$site=strtolower($site);

	if(file_exists($site)){
		include $site;
		return ;
	}

	//echo 'NO FILE('.$site.')';
	return false;
});
?>