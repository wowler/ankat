<?php
/*
encrypt
J_encode function powered by Jimpop 2012.05.17
0.8a
*/

class encrypt{
	public function __construct(){
		$this->psw=new j_encode;
		$this->mcrypt=new mcrypt;
	}
	public function hash(){
		$argus=func_get_args();

		$hash_pre=ANK_CONF::get('security')['hash_prefix'];

		foreach($argus as $inx => $val){
			$hash_pre.=$val;
		}

		$hash_pre=sha1($hash_pre);

		$hash=substr($hash_pre,0,15);
		
		return $hash;
	}
}
//one hash for password
class j_encode{
	//產生必須數範圍
	public function rand(){
		$code=rand(0,99999);

		return $code;
	}
	//確認密碼是否正確
	public function check($user,$psw,$code,$hash){
		/*
			user:
			psw:
			code:
			hash:compare_hash
		*/
		$user;
		$psw;
		$code;

		$hash;

		$tmp_hash=$this->encode($user,$psw,$code);

		if($tmp_hash==$hash){
			$re=true;
		}else{
			$re=false;
		}

		return $re;
	}
	public function encode($user,$psw,$code=-1){//產生驗證密碼
		/*
			user:
			psw:
			code:
		*/
		$user;
		$psw;
		$code=intval($code);

		if($code%35<10)$ecode=substr($this->hash_enc($code),0,10); //產生干擾碼
		else $ecode=substr($this->hash_enc($code),($code%5),($code%35));
		$userinfo=array($user,$psw,$ecode);

		$tmp1=$this->array_sort($userinfo,$code);
		$tmp2=$this->array_upset2($tmp1,$code);

		$hash=$this->hash_enc($this->array_sort($tmp2,($code+2)));

		return $hash;
	}
	private function array_upset2($tmp1,$code){
			$j=0; //目前分段陣列指標處
			$k=($code+1)%3; //目前字串取得指標處
			for($i=0;$i<ceil(strlen($tmp1)/3);$i++){ //第二次打亂
				for($i2=0;$i2<3;$i2++)$tmp2[($j+$i2)%3].=substr($tmp1,($i*3+($k+$i2)%3),1);
			}
			//print_r($tmp2);
			return $tmp2;
	}
	private function array_sort($str_arr,$code=-1){//排列組合函數
		if(count($str_arr)!=3)return "Error! number of count array is wrong!";
		if($code<0)$num=($str_arr[2]%6); //如果未設定code
		else $num=$code; //已設定code
		
		$j=0; //位置指標
		for($i=0;$i<3;$i++)
		{
		   $tmp[$j] = $str_arr[$i] . $str_arr[($i + 1) % 3] . $str_arr[($i + 2) % 3];
		   $tmp[$j + 1] = $str_arr[$i] . $str_arr[($i + 2) % 3] . $str_arr[($i + 1) % 3];
		   $j += 2;
		}
		return $tmp[$num%6];
	}
	private function hash_enc($str){
		return hash('sha512',$str);
	}
}
//encode & decode
class mcrypt{
	public function __construct(){
		$this->cipher=MCRYPT_RIJNDAEL_128;
		$this->mode=MCRYPT_MODE_CBC;
	}
	public function encode($string,$key){
		$hash_key=$this->make_key($key);

		$padding = 16 - (strlen($string) % 16);
		$string .= str_repeat(chr($padding), $padding);
		$encrypt = mcrypt_encrypt($this->cipher,$hash_key['key'],$string,$this->mode,$hash_key['iv']);

		$re = base64_encode($encrypt);

		return $re;
	}
	public function decode($encrypt_string,$key){
		$hash_key=$this->make_key($key);

		$string=base64_decode($encrypt_string);

		$data = mcrypt_decrypt($this->cipher,$hash_key['key'],$string,$this->mode,$hash_key['iv']);
		$padding = ord($data[strlen($data) - 1]);
		$re = substr($data, 0, -$padding);

		return $re;
	}
	/*
internal function 
	*/
	private function make_key($hash_string){
		$hash_string.=\ANK_CONF::get('security')['hash_prefix'];
		$hash = hash('SHA384', $hash_string, true);
		$app_cc_aes_key = substr($hash, 0, 32);
		$app_cc_aes_iv = substr($hash, 32, 16);

		$rd=[
			'key'=>$app_cc_aes_key,
			'iv'=>$app_cc_aes_iv
		];

		return $rd;
	}
}
?>