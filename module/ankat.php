<?php
/*
	NEW Version anKat
*/
class ANK{
	private static $defined,$itn_d,$git_obj;
	public static $input,$output,$loader,$html,$route,$encrypt,$cache,$security,$lib;
	public static $db,$session,$log,$tool,$lang,$file,$fdb,$arrDB,$rq_rec;
	public static function init(){
		self::$defined=false;

		self::$itn_d=array();
		
		if(\ANK_CONF::get('included_arr')!='error'){
			self::$itn_d['included_arr']=\ANK_CONF::get('included_arr');
		}else{
			self::$itn_d['included_arr']=array();
		}
		
		self::$itn_d['md_path']=__DIR__.'/';//module folder

		$server_config=self::load_config();

		self::$itn_d['server_config']=$server_config;//load server setting
		self::$itn_d['md_data']=self::$itn_d['server_config']['md'];//md datas

		\ANK_CONF::set(self::$itn_d['server_config']['md'],'md_config');//set others module variable

		//check if this domain is allowed
		$this_domain=self::get_this_domain(
			self::$itn_d['server_config']['server']['domain'],
			self::$itn_d['server_config']['server']['allow_others_domain']);

		if($this_domain===false){
			echo('400 Bad Request!Sorry we dont allow this domain');
			exit;
		}
		

		//system infos
		\ANK_CONF::set(self::$itn_d['server_config']['init_file'],'init_file');
		\ANK_CONF::set(self::$itn_d['server_config']['website'],'website');
		\ANK_CONF::set(self::$itn_d['server_config']['timezone'],'timezone');
		\ANK_CONF::set(self::$itn_d['server_config']['db'],'db');
		\ANK_CONF::set(self::$itn_d['server_config']['dev'],'dev');
		\ANK_CONF::set(self::$itn_d['server_config']['security'],'security');
		\ANK_CONF::set(self::$itn_d['server_config']['output_js_setting'],'output_js_setting');
		//set runnung os
		\ANK_CONF::set(strtoupper(self::$itn_d['server_config']['server']['os']),'os');
		//set PHP SESSION status
		\ANK_CONF::set(strtoupper(self::$itn_d['server_config']['server']['php_session']),'PHP_SESSION');
		//set request_record status
		\ANK_CONF::set(self::$itn_d['server_config']['request_record'],'request_record');

		$protocol="http://";
		if(isset($_SERVER['HTTPS'])){$protocol="https://";}
		$this_web_path="{$protocol}{$this_domain}";

		define("EXTMD_SITE",self::$itn_d['server_config']['server']['extend_site']);//Website extension modules relative position
		define("MD_SITE",self::$itn_d['server_config']['server']['module_site']);//The relative position of the module site
		define("ROOT_PATH",self::$itn_d['server_config']['server']['root_path']);//The absolute position of the main program
		define("WEB_SITE",$this_domain);//Domain names with major underlying
		define("WEB_PATH",$this_web_path);//Domain names and protocol
		define("EXT_PATH",ROOT_PATH.EXTMD_SITE);//The absolute position of the module site
		define("TMP_PATH",ROOT_PATH."module/tmp/");//The absolute position of the module site
		
		$cdn_setting=self::$itn_d['server_config']['server']['cdn'];
		if(count($cdn_setting)>0){
			for($i=0,$cnt=count($cdn_setting);$i<$cnt;$i++){
				$defined_name="CDN_DOMAIN_".$i;
				$cdn_domain=$cdn_setting[$i];
				define($defined_name,$cdn_domain);//CDN domain
			}
		}

		define("SERVER_INIT",true);//define server init ok

		self::$output=new \OUTPUT();
		self::$loader=new \LOADER();
		self::$html=new \HTML;
		self::$route=new \ROUTE;
		self::$encrypt=new \encrypt;
		self::$cache=new \cache;
		self::$security=new \SECURITY;
		self::$input=new \INPUT();
		self::$lib=new \LIB;
		self::$tool=new \TOOLS;
		//for default language charset
		$lang_default_charset=self::$itn_d['server_config']['lang']['default'];
		self::$lang=new \LANG($lang_default_charset);
		self::$file=new \FILE;
		self::$fdb=new \FDB;
		self::$arrDB=new \ARRDB;
		self::$rq_rec=new \RQ_REC;


		//LOAD GIT CORE
		self::$git_obj = new \GIT_CLIENT('_main',self::$itn_d['server_config']);
		
		self::$defined=true;//set init complete

		//load lib enable list
		$lib_enable_list=self::$itn_d['server_config']['lib_enable'];
		self::$lib->enable($lib_enable_list);
	}
	public static function git($file_name,$md_name,$git_user='_main'){
		$obj=self::$git_obj;

		if(strpos($md_name,'/')!==false){//if use auto set
			$rd=$obj->parser_site($md_name);
			$git_user=$rd['user'];
			$md_name=$rd['md'];
		}

		$re_site=$obj->gt($file_name,$md_name,$git_user);

		$md_site=$obj->md_site($git_user,$md_name);//return include need md

		return $re_site;
	}
	public static function json_load($site){
		if(file_exists($site)){
			$config_string=file_get_contents($site);
			$get_data=json_decode($config_string,true);
			$return=$get_data;
		}else{
			$return=false;
		}
		return $return;
	}
	//load config
	public static function load_config(){
		$path=self::$itn_d['md_path'].'ankat_config.php';

		$rd=include($path);//load server setting

		return $rd;
	}
	//allow domain set
	public static function get_this_domain($arr=[],$allow_others=false){
		/*

		*/
		

		$this_domain=strtolower($_SERVER['HTTP_HOST']);
		if(in_array($this_domain,$arr)){
			return $this_domain;
		}

		//if allow others domain then return this domain
		if($allow_others===true){
			return $this_domain;
		}

		return false;
	}
	//error display method
	public static function ERROR_DISPLAY(){
		$dev_status=\ANK_CONF::get('dev')['status'];
		if($dev_status==true){
			$error_level=(string)\ANK_CONF::get('dev')['error_level'];
			$err_dp=1;
			switch($error_level){
				case 'off':
					$err_rp=0;
					$err_dp=0;
					//if no display and no record system
					\ANK_LOG::set_record(false);
				break;
				case '2':
					$err_rp=E_ERROR;
				break;
				case '1':
					$err_rp=E_ERROR | E_PARSE;
				break;
				case '0':
				default:
					$err_rp=E_ERROR | E_WARNING | E_PARSE;
				break;
			}
			error_reporting($err_rp);
			ini_set("display_errors", $err_dp);
		}else{
			error_reporting(0);
			ini_set("display_errors", 0);
		}
	}
	//set setting about outputing JS setting to HTML
	public static function set_js_setting($status=true){
		\ANK_CONF::set($status,'output_js_setting');
	}
	//get basic setting for HTML or JS
	public static function make_basic_setting(){
		$setting=[
			'base_url'=>WEB_SITE
		];

		return $setting;
	}
}

class ANK_CONF{
	public static $datas;
	public static function set($data,$type='default'){
		if(gettype(self::$datas)!='array'){
			self::$datas=array();
		}
		self::$datas[$type]=$data;
	}
	public static function get($type='default'){
		if($type=='default'){//if no set variable,then set default
			return self::$datas;
		}else{
			if(isset(self::$datas[$type])){//if set type,then return dat of type
				return self::$datas[$type];
			}
		}

		return 'error';
	}
}
/*
	load core
*/
$root_path=__DIR__.'/core/';
$include_core=array(
		'input.php',
		'AJAX.php',
		'GIT_Client.php',
		'debug.php',
		'tools.php',
		'mvc.php',
		'output.php',
		'loader.php',
		'html.php',
		'route.php',
		'encode.php',
		'API.php',
		'cache.php',
		'security.php',
		'global.php',
		'lib/lib.php',
		'lang.php',
		'file.php',
		'fdb.php',
		'arrDB.php',
		'request_records.php',
	);
foreach ($include_core as $key => $value){
	$r_site=$root_path.$value;
	include $r_site;
}

\ANK::init();
?>