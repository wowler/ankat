<?php
/*
	TINY anKat SERVER
*/
//print_r(ANK_TINY::get('init_file'));
//print_r(ANK_TINY::get('md'));

class ANK_TINY{
	public static $server_config,$input,$init;
	public static function init(){

		$md_path=__DIR__.'/';//模組資料夾

		$server_config=include($md_path.'ankat_config.php');//load 
		
		self::$server_config =$server_config;

		define("EXTMD_SITE",$server_config['server']['extend_site']);//延伸模組的網站相對位置
		define("MD_SITE",$server_config['server']['module_site']);//模組的網站相對位置
		define("ROOT_PATH",$server_config['server']['root_path']);//主要上層的絕對位置
		define("WEB_SITE",$server_config['server']['domain']);//網域名與主要底層
		define("EXT_PATH",ROOT_PATH.EXTMD_SITE);//模組的網站絕對位置

		self::$input=new INPUT();

		define("SERVER_INIT",true);//告知伺服器已設定完成

		return true;
	}
	public static function get($type=''){
		/*

		*/
		if(!isset(self::$init)){
			self::init();
		}

		$rd=static::$server_config[$type];

		return $rd;
	}
	public static function json_load($site){//JSON LOAD FILE
		if(file_exists($site)){
			$config_string=file_get_contents($site);
			$get_data=json_decode($config_string,true);
			$return=$get_data;
		}else{
			$return=false;
		}
		return $return;
	}
}

$root_path=__DIR__.'/core/';
$include_core=array(
		'input.php',
		'security.php',
		'db_session.php',
		'debug.php'
	);
foreach ($include_core as $key => $value){
	$r_site=$root_path.$value;
	include $r_site;
}
?>