<?php
$fix_title=\ANK_CONF::get('website')['fix_title'];//固定標題
$title='sample - '.$fix_title;//網頁標題

//new ANK version route
if(count($input_arr)>0){
  $ct_url=$input_arr[0];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Language" content="zh-tw">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php echo $title;?></title>
<link href="/bootstrap/css/bootstrap.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="bootstrap/assets/js/html5shiv.js"></script>
    <script src="bootstrap/assets/js/respond.min.js"></script>
  <![endif]-->
<script src="/lib/jquery-1.11.1.min.js"></script>
</head>
<style>
html,body{height:100%;}
</style>
<body>
	<div class="container">
		<?php require EXT_PATH.'_main/home/nav.php';//nav?>
		<div style="padding-top:75px;">
			<?php require $ct_url;//Content?>
		</div>
		<hr class="featurette-divider">
		<?php require EXT_PATH.'_main/home/footer.php';//Footer?>
	</div>
</body>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="/js/server_main.js"></script>
<?php ANK::$output->css_render('css/index.css','home','_main');?>
<?php
echo \ANK::$output->buffer->output('js');
echo \ANK::$output->buffer->output('css');
?>
</html>