<?php
$ct_url=$input_arr[0];
?>
<html>
<head>
	<meta http-equiv="Content-Language" content="zh-tw">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>ANKat System</title>
	<script src="/lib/jquery-1.11.1.min.js"></script>
	<script src="/js/server_main.js"></script>
</head>

<body>
	<?php require $ct_url;?>
</body>
<?php
echo \ANK::$output->buffer->output('js');
echo \ANK::$output->buffer->output('css');
?>
</html>