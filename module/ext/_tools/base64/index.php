<?php
\ANK::$output->buffer->stack('js',['js/base64.js','base64','_tools']);
?>
<script language="javascript">
var gl={
	'init':function(){
		$('textarea[name="output"]').click(function(){
			$(this).select();
		});
	},
	'encode':function(){
		var obj=$('#base64');
		var input=obj.find('textarea[name="input"]');
		var out=obj.find('textarea[name="output"]');
		out.val(Base64.encode(input.val()));
		gl._uptime();
	},
	'decode':function(){
		var obj=$('#base64');
		var input=obj.find('textarea[name="input"]');
		var out=obj.find('textarea[name="output"]');
		out.val(Base64.decode(input.val()));
		gl._uptime();
	},
	'_uptime':function(){
		var obj=$('#base64');
		obj.find('.result_dtime').html(dt.get_now());
	}
}
var dt={
	'init':function(){

	},
	'get_now':function(){
		var dt=new Date();
		var dtime=(dt.getMonth()+1)+'月'+dt.getDate()+'日 '+dt.getHours()+':'+dt.getMinutes()+':'+dt.getSeconds();
		return dtime;
	}
}
$(document).ready(function(){
	gl.init();
});
</script>
<style>
.inline{display:inline;}

textarea.base64_ta{width:90%;height:350px;}
</style>
<div id="base64">
	<h2>BASE64 Tool</h2>
	<div>
		<div class="inline">更新時間：</div>
		<div class="result_dtime inline"></div>
	</div>
	<div>
		<textarea class="base64_ta" name="input"></textarea>
	</div>
	<div>
		<button onclick="gl.encode();">Encode</button>
		<button onclick="gl.decode();">Decode</button>
	</div>
	<div>
		<textarea class="base64_ta" name="output"></textarea>
	</div>
</div>