<?php
/*
	make-user
*/

$userid = (ANK::$input->post('userid'));
$psw = (ANK::$input->post('psw'));

if(strlen($userid)>0){
	$rand=ANK::$encrypt->psw->rand();
	$hash_psw=ANK::$encrypt->psw->encode($userid,$psw,$rand); 
}


?>
<div class="content">
	<div class="row">
		<div class="col-md-6">
			<?php echo $userid.'('.strlen($userid).')  PSW:('.strlen($psw).') <br/>';?>
			<form action="?t=tool-make-user" method="POST">
			<table class="table table-bordered">
				<tr>
					<th>Userid</th>
					<td><input type="text" name="userid" class="form-control" value="<?php echo $userid;?>"></td>
				</tr>			
				<tr>
					<th>Password</th>
					<td><input type="password" name="psw" class="form-control" value="<?php echo $psw;?>"></td>
				</tr>
				<tr>
					<td colspan="2">
						<button class="btn btn-primary">送出</button>
					</td>
				</tr>
			</table>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h4>HASH</h4>
			<input type="text" class="form-control" name="hash" value="<?php echo $hash_psw;?>">			
			<h4>CODE</h4>
			<input type="text" class="form-control" name="code" value="<?php echo $rand;?>">
		</div>
	</div>
</div>