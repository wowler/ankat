<?php

function make_dt_to_unix(){
	$html=<<<HTML
	<div class="dt_unix_unit">
		<div class="form-inline">
			<input type="text" class="form-control" name="dt" placeholder="yyyy-mm-dd HH:ii:ss">
			<button class="btn btn-danger btn_conv_dt_unix">CONV</button>
			<input type="text" class="form-control" name="result">
		</div>
	</div>
HTML;
	return $html;
}
function make_unix_to_dt(){
	$html=<<<HTML
	<div class="unix_dt_unit">
		<div class="form-inline">
			<input type="text" class="form-control" name="unix" placeholder="">
			<button class="btn btn-danger btn_conv_unix_dt">CONV</button>
			<input type="text" class="form-control" name="result">
		</div>
	</div>
HTML;
	return $html;
}
?>
<style>
.dt_unix_unit,.unix_dt_unit{margin:6px;}
</style>
<div>
	<h3>UNIXTIME CONVERT</h3>
	<h4>datetime to UNIX</h4>
	<div>
		<?php echo make_dt_to_unix();?>
		<?php echo make_dt_to_unix();?>
		<?php echo make_dt_to_unix();?>
		<?php echo make_dt_to_unix();?>
		<?php echo make_dt_to_unix();?>
	</div>
	<h4>UNIX to datetime</h4>
	<div>
		<?php echo make_unix_to_dt();?>
		<?php echo make_unix_to_dt();?>
		<?php echo make_unix_to_dt();?>
		<?php echo make_unix_to_dt();?>
		<?php echo make_unix_to_dt();?>
	</div>
</div>
<script>
var unix=(function(){
	this.init=function(){
		this.btn_bind();
		var nid=0;
		$('.dt_unix_unit').each(function(i,v){
			var obj=$(v);
			obj.attr({
				'nid':nid
			});
			obj.find('input,button').attr({
				'nid':nid
			});
			nid++;
		});
		$('.unix_dt_unit').each(function(i,v){
			var obj=$(v);
			obj.attr({
				'nid':nid
			});
			obj.find('input,button').attr({
				'nid':nid
			});
			nid++;
		});
	}
	this.btn_bind=function(){
		$('.btn_conv_dt_unix').click(function(){
			var nid=$(this).attr('nid');

			var obj=$('.dt_unix_unit[nid="'+nid+'"]');

			var timeoffset=new Date().getTimezoneOffset();

			var sd={
				'dt':obj.find('input[name="dt"]').val(),
				'timeoffset':timeoffset
			}

			sd['ajax_type']='conv_dt_to_unix';

			ANK.ajax(['ajax/ajax_unixtime.php','unixtime','_tools'],sd,function(gdata){
				var re=gdata['re'];

				obj.find('input[name="result"]').val(re);
			},'json');
		});
		$('.btn_conv_unix_dt').click(function(){
			var nid=$(this).attr('nid');

			var obj=$('.unix_dt_unit[nid="'+nid+'"]');

			var timeoffset=new Date().getTimezoneOffset();

			var sd={
				'unix':obj.find('input[name="unix"]').val(),
				'timeoffset':timeoffset
			}

			sd['ajax_type']='conv_unix_to_dt';

			ANK.ajax(['ajax/ajax_unixtime.php','unixtime','_tools'],sd,function(gdata){
				var re=gdata['re'];

				obj.find('input[name="result"]').val(re);
			},'json');
		});
	}
	this.init();
	var re={

	}

	return re;
}());
</script>