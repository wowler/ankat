<?php
/*

*/
$t=new ajax_unixtime;
$d=\ANK::$input->post();
$t->put($d);

class ajax_unixtime extends \AJAX{
	public function conv_dt_to_unix($d=[]){
		/*
			timeoffset:
			dt:input datetime	
		*/
		$t1=strtotime($d['dt']." UTC");
		$timezone_offset=intval($d['timeoffset'])*60;
		$t1+=$timezone_offset;

		$sd=[
			're'=>$t1
		];

		return $this->output($sd);
	}
	public function conv_unix_to_dt($d=[]){
		/*
			timeoffset:
			unix:input datetime	
		*/
		$t1=intval($d['unix']);
		$timezone_offset=intval($d['timeoffset'])*60;
		

		$re=date('Y/m/d H:i:s',$t1);
		$t1=strtotime("{$re} UTC");
		$t1+=$timezone_offset;

		$re=date('Y/m/d H:i:s',$t1);

		$sd=[
			're'=>$re
		];

		return $this->output($sd);
	}
}
?>