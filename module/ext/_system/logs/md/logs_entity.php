<?php
/*

*/
namespace _system\logs;

class logs_entity{
	public function __construct(){
		$this->rep = new logs_rep;
	}
	//get access logs
	public function get_access_logs($d=[]){
		/*

		*/
		$log_path=dirname(ROOT_PATH)."/logs/";
		$dtime=date('Ymd');
		$filename="access.{$dtime}.log";

		$file_path=$log_path.$filename;

		$cnt=50;

		$rd=shell_exec("tail -n{$cnt} {$file_path}");
		$rd = array_filter(preg_split('#[\r\n]+#', trim($rd)));
		rsort($rd);

		return $rd;
	}
	//get error logs
	public function get_error_logs($d=[]){
		/*

		*/
		$log_path=dirname(ROOT_PATH)."/logs/";
		$dtime=date('Ymd');
		$filename="error.{$dtime}.log";

		$file_path=$log_path.$filename;

		$cnt=50;

		$rd=shell_exec("tail -n{$cnt} {$file_path}");
		$rd = array_filter(preg_split('#[\r\n]+#', trim($rd)));
		rsort($rd);

		$sd=[];
		\DEBUG::set('tt','tt');
		foreach($rd as $key => $val){
			//process datetime to unix time
			preg_match_all("/(\[.+?.\])/", $val,$r);
			$datetime=str_replace(['[',']'],'',$r[0][0]);//datetime
			$datetime=preg_replace("/\.\d*/",'',$datetime);//datetime
			$datetime=strtotime($datetime);

			$php_error_type = $r[0][1];//type (:error)
			$pid = $r[0][2];//pid (pid 2255)
			$from_where = $r[0][3];//from where (client 192.168.1.1:12335)

			//process script site
			preg_match_all("/(PHP.+?.\:).(.*?.) in (.+.)/", $val,$r2);
			$chk_type=count($r2[0]);
			//if is default
			if($chk_type==1){
				$php_type = $r2[1][0];//type (PHP Notice)
				$php_type = str_replace('PHP','',$php_type);
				$reason = $r2[2][0];//reason (Use of undefined constant self - assumed 'self')
				$script_site = $r2[3][0];//script site (xxx on line 31.referer url)
			}else{
				//try if custom
				preg_match_all("/.*.\].(.+?.)\,(.*.)/", $val,$r2);
				$php_type = '{{CUSTOM}}';
				$reason = $r2[1][0];
				$script_site = $r2[2][0];
			}

			$sd[]=[
				'datetime'=>$datetime,
				'php_error_type'=>$php_error_type,
				'pid'=>$pid,
				'from_where'=>$from_where,
				'php_type'=>$php_type,
				'reason'=>$reason,
				'script_site'=>$script_site,
				'debug'=>($r2),
				'source'=>$val
			];
		}

		return $sd;
	}
}
?>