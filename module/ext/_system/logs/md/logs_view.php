<?php
/*

*/
namespace _system\logs;

class logs_view{
	//make init html content
	public function make_init_html(){
		$html=<<<HTML
		<div class="row">
			<button class="ank_click" method="function" fun_cal="logs.get_new">GET NEW</button>
			<div class="col-md-12">
				<h3>ERROR LOGS</h3>
			</div>
			<div class="error_log_list col-md-12">
				
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3>ACCESS LOGS</h3>
			</div>
			<div class="access_log_list col-md-12">
				
			</div>
		</div>
HTML;
		return $html;
	}
	public function make_hidden_units(){
		$html=<<<HTML
		<div id="hidden_units" style="display:none;">
			<!-- error unit-->
			<div class="error_log_unit">
				<span class="time"></span>&nbsp;
				<span class="date"></span>
				<span class="php_error_type"></span>
				<span class="pid"></span>
				<span class="from_where"></span>
				<span class="php_type"></span>
				<span class="reason"></span>
				<span class="script_site"></span>
			</div>
			<!-- Dividers -->
			<div class="dividers">
				<span class="content"></span>
			</div>
			<!-- -->
			<div class="warning_icon">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAGySURBVEhL5ZU7SwNBFIVHTLIxiraCjYUIQiIkiIWlhVYaLMQnliqIBrUTH4i1oOA/sBD8BxJxd9OoYGMpiJYWIgTRQtS5nlmuEiazySam0gNfM/eeM3dnh13xf0THol661i7YJhJ1vFwbkR1rlW7kjFyLFNKxjuhKxLhcvcgWIXKsWYQ/focXbHKDpxng1uCSF6JZuuEUQtfBvR6sg40upW0tSjscx0BNHOMvTPVuCgqC8nKMv9CY140VkOcYf+FYbg1Gj7nBHg9TTaG8HOMvNP3cFp2loRRl0iljTaG8HOMvnOOeyaxYTidpZSRprCng3ecYf8lceNxkVqyNdXuYagqZi05zjL/kaUMbrt6nKWBjIkFbU4midQU8kmyrnWNKi5yIYwrZmYl7mGo4/3O2lxfOct4UUgrpRDJsLy95Ihox0YMecrDQ6aGv43ieKCta2B5M6hukB21OJjz0dUy/yrbgUh86THath+kg/A69UbZVJpi78D5eTMEKDPAm3VAvt1cnbNCPoNeicNf6wG0b5bbfSbrRvsKXjg2f8XTDXK6NvL+aEznERll8/zt4+c9LiC9J82+jLG7IuwAAAABJRU5ErkJggg==">
			</div>
		</div>
HTML;
		
		return $html;
	}
}
?>