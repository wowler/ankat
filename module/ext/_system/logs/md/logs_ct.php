<?php
namespace _system\logs;

class logs_ct{
	public function __construct(){
		$this->entity = new logs_entity;
		$this->view = new logs_view;
	}
	public function make_init_html(){
		$html=$this->view->make_init_html();
		$html.=$this->view->make_hidden_units();

		return $html;
	}
	//get new logs
	public function get_new($d=[]){
		/*
			
		*/
		//get access logs
		$sd=$d;
		$rd_access=$this->entity->get_access_logs($sd);
		//get error logs
		$rd_error=$this->entity->get_error_logs($sd);

		$sd=[
			'access'=>$rd_access,
			'error'=>$rd_error
		];

		return $sd;
	}
}
?>