var logs=(function(){
	this.init=function(){
		this.btn_bind();
	}
	this.btn_bind=function(){

	}

	this.init();

	var re={
		'get_new':function(){
			var data={};
			data['ajax_type']='get_new';
			this.error_less_2min=false;
			this.error_over_2min=false;
			$('.error_log_list,.access_log_list').html('');

			ANK.ajax(['ajax/ajax_logs.php','logs','_system'],{'data':data},function(gdata){
				
				if(gdata['status']){
					var access=gdata['d']['access'];
					$('.access_log_list').html(access.join('<br />'));

					//ERROR process
					logs.process_error_logs(gdata['d']['error']);
					//$('.error_log_list').html(error.join('<br />'));
				}
			},'json');
		},
		'process_error_logs':function(error_data){
			var now=(Date.now() / 1000 | 0);

			for(var i=0,cnt=error_data.length;i<cnt;i++){
				var td=error_data[i];
				var hobj=$('#hidden_units .error_log_unit').clone();

				if(now-td['datetime']<120 && logs.error_less_2min==false){
					var h2=$('#hidden_units .dividers').clone();
					h2.find('.content').html('~~LESS 2 min~');
					h2.addClass('error_log_less_2min');
					h2.appendTo($('.error_log_list'));

					logs.error_less_2min=true;
				}
				if(now-td['datetime']>120 && logs.error_over_2min==false){
					var h2=$('#hidden_units .dividers').clone();
					h2.find('.content').html('~~OVER 2 min~');
					h2.addClass('error_log_over_2min');
					h2.appendTo($('.error_log_list'));

					logs.error_over_2min=true;
				}
				
				var dt = timeConverter(td['datetime']);
				var date=dt['date'];
				var time=dt['time'];

				var php_error_type=td['php_error_type'];
				var pid=td['pid'];
				var from_where=td['from_where'];
				var php_type=td['php_type'];
				var reason=td['reason'];
				var script_site=td['script_site'];

				if(php_type=='{{CUSTOM}}'){
					php_type=$('#hidden_units .warning_icon').clone().html();
				}

				hobj.find('.date').html(date);
				hobj.find('.time').html(time);
				hobj.find('.php_error_type').html(php_error_type);
				hobj.find('.pid').html(pid);
				hobj.find('.from_where').html(from_where);
				hobj.find('.php_type').html(php_type);
				hobj.find('.reason').html(reason);
				hobj.find('.script_site').html(script_site);
				hobj.attr('title',td['source']);
				hobj.appendTo($('.error_log_list'));
			}
		}
	}

	return re;
}());

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var time=a.toLocaleTimeString();
  var date=a.toLocaleDateString();

  var r={
  	'date':date,
  	'time':time
  };

  return r;
}