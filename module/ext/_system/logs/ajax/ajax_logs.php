<?php
namespace _system\logs;

\MD::set('ajax_logs',new ajax_logs);
\MD::ajax_logs()->put($data);

class ajax_logs extends \AJAX{
	public function __construct(){
		$this->ct = new logs_ct;
	}
	public function get_new($d=[]){
		$rd=$this->ct->get_new($d);

		$desc_str="some error happend!";
		$status=true;
		if($status){
			$desc_str="get_new!";
		}

		$sdata=[
			'status'=>$status,
			'desc_str'=>$desc_str,
			'd'=>$rd
		];

		return $this->output($sdata);
	}
}
?>