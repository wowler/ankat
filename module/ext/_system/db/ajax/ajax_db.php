<?php
namespace _system\db;

\MD::set('ajax_db',new ajax_db);
\MD::ajax_db()->put($data);

class ajax_db extends \AJAX{
	public function __construct(){
		$this->ct = new db_ct;
	}
	public function backup($d=[]){
		$re=$this->ct->backup_db();

		$desc_str="some error happend!";
		if($re){
			$desc_str="backup complete!";
		}

		$sdata=[
			'status'=>$re,
			'desc_str'=>$desc_str
		];

		return $this->output($sdata);
	}
}
?>