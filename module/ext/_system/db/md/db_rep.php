<?php
/*

*/
namespace _system\db;

class db_rep{
	public function get_tables_list(){
		$raw="SHOW TABLES";

		$rd=\ANK::$db->raw($raw)->get();

		return $rd;
	}
	public function get_table($d=[]){
		/*
			table_name:
		*/
		$raw="SHOW CREATE TABLE {$d['table_name']}";

		$rd=\ANK::$db->raw($raw)->get();

		return $rd;
	}
	public function get_now_db(){
		$raw="SELECT DATABASE() AS `DB`;";

		$rd=\ANK::$db->raw($raw)->get();

		return $rd;
	}
}
?>