<?php
/*

*/
namespace _system\db;

class db_view{
	//make init html content
	public function make_init_html(){
		$html=<<<HTML
		<div class="">
			<button class="" onclick="db.backup()">Click to backup DB structure</button>
		</div>
		<div class="result">
			
		</div>
HTML;
		return $html;
	}
	//make db header
	public function make_backup_header($d=[]){
		/*
			datetime:
			database:
			host:
			query_string:
		*/

		$html=<<<HTML
-- ANKat dump DB structure for git
--
-- Host: {$d['host']}    Database: {$d['database']}
-- ------------------------------------------------------
-- Datetime {$d['datetime']}

{$d['query_string']}
HTML;
		return $html;
	}
}
?>