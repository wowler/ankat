<?php
namespace _system\db;

class db_ct{
	public function __construct(){
		$this->entity = new db_entity;
		$this->view = new db_view;
	}
	public function backup_db($d=[]){
		/*
			type: structure(default)
		*/
		//get all tables list
		$rd=$this->entity->get_tables_list();
		//\DEBUG::stack($rd,'rd');
		$query_string='';
		//foreach every table create structure
		foreach($rd as $key => $val){
			$rds=$this->entity->get_table([
				'table_name'=>$val
			]);
			$query_string.="{$rds};\r\n";
		}

		//get now database


		$datetime=date('Y/m/d H:i:s e');
		$database=$this->entity->get_now_db();
		$host=\ANK_CONF::get('db')['connection']['read']['host'];

		$result=$this->view->make_backup_header([
			'datetime'=>$datetime,
			'database'=>$database,
			'host'=>$host,
			'query_string'=>$query_string
		]);

		$file_path=ROOT_PATH."/_DB/{$database}-structure.sql";

		$re=\ANK::$file->put($file_path,$result);

		if($re!==false){
			$re=true;
		}

		return $re;
	}
	public function make_init_html(){
		$html=$this->view->make_init_html();

		return $html;
	}
}
?>