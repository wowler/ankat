<?php
/*

*/
namespace _system\db;

class db_entity{
	public function __construct(){
		$this->rep = new db_rep;
	}
	//
	public function get_tables_list(){
		/*

		*/
		$rd = $this->rep->get_tables_list();

		$sd=[];
		//proccess data format
		foreach($rd as $key => $val){
			$key=array_keys($val);
			$sd[]=$val[$key[0]];
		}

		return $sd;
	}
	public function get_table($d=[]){
		/*
			table_name:
		*/
		$rd=$this->rep->get_table($d)[0];

		//proccess data format
		$sd=$rd['Create Table'];
		//replace AUTO_INCREMENT to 0
		$find="/AUTO_INCREMENT=(\d*?) DEFAULT/";
		$replace="AUTO_INCREMENT=0 DEFAULT";
		$r=preg_replace($find,$replace,$sd);
		

		return $r;
	}
	public function get_now_db(){
		$rd=$this->rep->get_now_db()[0];

		//process data format
		$sd=$rd['DB'];

		return $sd;
	}
}
?>