<?php
namespace _system\rq_rec;

\MD::set('ajax_rq_rec',new ajax_rq_rec);
\MD::ajax_rq_rec()->put($data);

class ajax_rq_rec extends \AJAX{
	public function __construct(){
		$this->ct = new rq_rec_ct;
	}
	public function get_new($d=[]){
		/*

		*/
		//get data from ct
		$rd=$this->ct->get_new($d);

		$sd=[
			'status'=>true,
			'desc_str'=>'',
			'd'=>$rd
		];

		return $this->output($sd);
	}
	public function get_unit($d=[]){
		/*
			uid:
			keyword:
		*/
		//get data from ct
		$rd=$this->ct->get_unit($d);

		$sd=[
			'status'=>true,
			'desc_str'=>'',
			'd'=>$rd
		];

		return $this->output($sd);
	}
	//request again
	public function request_again($d=[]){
		/*
			uid:
		*/
		$uid=$d['uid'];
		//get data from ct
		$sd=[
			'uid'=>$uid
		];
		$rd=$this->ct->get_unit($sd);

		$status=false;

		//if data is exist
		if(strlen($rd['uri'])>0){
			//headers
			$headers_tmp=$rd['d']['client_headers'];
			$headers_tmp=json_decode($headers_tmp,true);
			$headers=[];
			foreach($headers_tmp as $key => $val){
				//if header unit is content-length then remove
				if(strtolower($key)=='content-length'){
					continue;
				}
				//if header unit is ank-track-id
				if(strtolower($key)=='ank-track-id'){
					continue;
				}
				$headers[]="{$key}:{$val}";
			}
			$t_rand=sha1(time().rand(100000,999999));
			$rand=substr($t_rand,0,8);
			$track_id="ank-rq-ag_{$rand}";
			$headers[]="ank-track-id:{$track_id}";
			//body
			$body=$rd['d']['raw_input_ori'];
			$sd=[
				'url'=>$rd['d']['endpoint'],
				'method'=>$rd['d']['method'],
				'headers'=>$headers,
				'body'=>$body,
			];

			//$debug[]=($headers_tmp);

			$re=$this->ct->entity->curl_remote($sd);
			if($re['status']==true){
				$status=true;
			}

			//$debug[]=$re;
		}

		$sd=[
			'status'=>true,
			'desc_str'=>'',
			'track_id'=>$track_id
		];

		$sd['debug']=$debug;

		return $this->output($sd);
	}
}
?>