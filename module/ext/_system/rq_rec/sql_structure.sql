SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `request_records` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uid` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `track_id` varchar(24) COLLATE utf8_bin DEFAULT NULL,
  `method` char(8) COLLATE utf8_bin DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


ALTER TABLE `request_records`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `request_records`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;
