<?php
/*

*/
namespace _system\rq_rec;

class rq_rec_entity{
	public function __construct(){
		$this->rep = new rq_rec_rep;
	}
	public function get_new($d=[]){
		/*
			keyword
		*/
		//GET FROM DB
		$rd=$this->rep->get_list($d);

		//get from files
		//foreach($rd as $key => $val){
		//	$rd[$key]['d']=$this->get_file_path(['id'=>$val['id']]);
		//}

		//process datas
		foreach($rd as $key => $val){
			//$rd[$key]['dtime']=date('Y/m/d H:i:s',$val['timestamp']);
		}

		return $rd;
	}
	public function get_unit($d=[]){
		/*
			uid
		*/
		//GET FROM DB
		$trd=$this->rep->get_list($d);

		if(count($trd)<1){
			return false;
		}

		$rd=$trd[0];//only one unit

		$rd['d']=$this->get_file_path(['id'=>$rd['id']]);

		$rd['d']['post_data']=json_encode($rd['d']['post_data'],JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
		$rd['d']['client']=json_encode($rd['d']['client'],JSON_PRETTY_PRINT);
		$rd['d']['client_headers']=json_encode($rd['d']['client_headers'],JSON_PRETTY_PRINT);

		//if server response is json then parser to JSON format
		if($this->is_json($rd['d']['server_response'])){
			$rd['d']['server_response']=json_decode($rd['d']['server_response']);
			$rd['d']['server_response']=json_encode($rd['d']['server_response'],JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
		}

		//if input is json then parser to JSON format
		$rd['d']['raw_input_ori']=$rd['d']['raw_input'];//storage ori datas
		if($this->is_json($rd['d']['raw_input'])){
			$rd['d']['raw_input']=json_decode($rd['d']['raw_input']);
			$rd['d']['raw_input']=json_encode($rd['d']['raw_input'],JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
		}

		return $rd;
	}
	//request remote
	public function curl_remote($d=[]){
		/*
			url:
			method:(string)
			headers:(array)
			body:(text)
		*/
		$url=$d['url'];
		$method=$d['method'];
		$headers=$d['headers'];
		$body=$d['body'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 1,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYHOST => false,
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $method,
		  CURLOPT_HTTPHEADER => $headers,
		  CURLOPT_POSTFIELDS => $body,
		));

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);
		
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return  [
		  	'status'=>false,
		  	'error'=>$err
		  ];
		}
		 
		return $response;
	}
/*
	internal function
*/
	private function get_file_path($d=[]){
		/*
			id:(from DB request_records.id)
		*/

		$base_path=ROOT_PATH."/storage/request_records/";
		$folder_name=ceil($d['id']/1000)*1000;

		$file_path="{$base_path}{$folder_name}/{$d['id']}.rec";

		$rd=\ANK::$file->get($file_path,'JSON');

		return $rd;
	}
	private function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
}
?>