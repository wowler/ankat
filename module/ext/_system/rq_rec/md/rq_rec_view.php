<?php
/*

*/
namespace _system\rq_rec;

class rq_rec_view{
	//make init html content
	public function make_init_html(){
		$html=<<<HTML
		<div class="row">
			<h3>REQUEST RECORDS</h3>
		</div>
		<div class="nav_bar row">
			<button class="ank_click" method="function" fun_cal="rq_rec.get_new_list">GET NEWER LIST</button>
			<span>-- OR --</span>
			<input type="text" name="keyword">
			<button class="ank_click btn_search" method="function" fun_cal="rq_rec.search_keyword">Search</button>
		</div>
		<hr>
		<div class="result">
			<ul class="rq_list"></ul>
		</div>
HTML;
	$hidden_html=<<<HTML
	<div class="hidden_rq" style="display:none;">
		<!-- menu_unit -->
		<div class="rq_unit">
			<li class="ank_click" stype="index" method="function" fun_cal="rq_rec.get_detail">
				<div>
					<span class="dtime"></span>
					<span class="track_id"></span>
					<span class="uid"></span>
					<span class="method"></span>
					<span class="uri"></span>
				</div>
			</li>
			<li stype="content" style="display:none;">
				<div style="text-align:left;">
					<button class="ank_click btn_close" method="function" fun_cal="rq_rec.hide_unit">close content</button>
					<button class="ank_click btn_rq_again" method="function" fun_cal="rq_rec.request_again">request again</button>
					<button class="ank_click btn_rq_just" method="function" style="display:none;" fun_cal="rq_rec.view_just_requested" track_id="">view just requested</button>
				</div>
				<h4>REQUEST ID(UID)</h4>
				<div class="uid">
					<span class="master"></span>
					<span class="nbs"></span>
				</div>
				<h4>LINK</h4>
				<div class="link_area">
					<input type="text" class="link_text">
					<a class="ank_click" method="function" fun_cal="rq_rec.copy_to_clip">
						<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAHuSURBVEhLrda7SxxRHMXxDTE2aRQMPmIqa9GgYgwGkoBFUkhSRu18FdooIgREmygYK5WQkELyZ4S0io1FiMEXvtFORRstTHx8j+4PrpeZ2Z3dPfBhcXbnnnHm3plJ5CBFGMAvLGIOYyhHzvIOh7gKcIY2ZJ0OXCCoxPzHW2QclVzCHXAGXRjHKey7DdxH7PglJ3gFN29g30sjYiWo5DmCsgT7Xbc2pJsnGMQo5hFVoqzDinRKM86j5GdQGmAlktap0zoZwiy28RffUYmgPMYmrGQXKSfDexzDPTqjqa1T6aYMa3B/9wGR6YR74cNUQynFKtzvphAZf3ad4wu00odh/6XWTwVKsJLcZr7iHkLjl2jQZ3CjU/QRWj/FWIZbooOKVaIpXIsqaBJoQtTA8hT+6ZpG7JI6KD9g2/9hAb/h3+tSljTD3Ukl9bDoINwBg0wgsqQQR7AdVKIF56cdB3AHl320IGX6YTtpFr1GWB5CzyBNhD40IR9p5SesSNciLJppvSi4+es2ecnPtPIHVhR2l3VXvK7lDkYQeU38aMpa0Wdt8KJ7l39b+YZYJcon2AB6Mr6ERTNvC1mXKDpivUjYQFpLWun+QpSMSyytSPWiMYmsSix6zrvPEbMHHUhO8wB6Gmr29eAFMnqDuZtE4hrc4t4SKukH+gAAAABJRU5ErkJggg==" style="width:18px;">
					</a>
				</div>
				<h4>ENDPOINT</h4>
				<div>
					<span class="method"></span> 
					<input type="text" class="endpoint">
				<div>
				<h4>POST DATA</h4>
				<div class="post_data">
					<textarea style="height:150px;"></textarea>
				</div>
				<h4>RAW INPUT</h4>
				<div class="raw_input">
					<textarea style="height:150px;"></textarea>
				</div>
				<h4>SERVER RESPONSE</h4>
				<div class="server_response">
					<textarea style="height:250px;"></textarea>
				</div>
				<h4>CLIENT HEADERS</h4>
				<div class="client_headers">
					<textarea style="height:250px;"></textarea>
				</div>
				<h4>CLIENT INFO</h4>
				<div class="client">
					<textarea style="height:250px;"></textarea>
				</div>
			</li>
		</div>
	</div>
HTML;
	$html.=$hidden_html;

		return $html;
	}
}
?>