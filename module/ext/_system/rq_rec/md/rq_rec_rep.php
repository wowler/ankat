<?php
/*

*/
namespace _system\rq_rec;

class rq_rec_rep{
	public function get_list($d=[]){
		/*
			uid
			keyword
		*/
		//
		$db=\ANK::$db->table('request_records');

		if(strlen($d['uid'])>0){
			$db->where([
				['uid',$d['uid']]
			]);
		}
		if(strlen($d['keyword'])>0){
			$db->where([
				['uid','LIKE',"%{$d['keyword']}%"]
			]);
			$db->orwhere([
				['track_id','LIKE',"%{$d['keyword']}%"]
			]);
			$db->orwhere([
				['uri','LIKE',"%{$d['keyword']}%"]
			]);
		}

		$db->orderby([
			['timestamp','DESC']
		]);

		$db->limit('0','100');

		$rd=$db->get();

		return $rd;
	}
}
?>