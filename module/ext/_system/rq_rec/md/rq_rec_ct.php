<?php
namespace _system\rq_rec;

class rq_rec_ct{
	public function __construct(){
		$this->entity = new rq_rec_entity;
		$this->view = new rq_rec_view;
	}
	public function make_init_html(){
		$html=$this->view->make_init_html();

		return $html;
	}
	public function get_new($d=[]){
		/*
			keyword
		*/
		//get from DB
		$rd=$this->entity->get_new($d);

		return $rd;
	}
	public function get_unit($d=[]){
		/*
			uid
		*/
		//get from DB
		$rd=$this->entity->get_unit($d);

		return $rd;
	}
}
?>