var rq_rec=(function(){
	this.init=function(){
		this.btn_bind();
		var url_path=ANK.fun.get_path();
		//if link with uid
		if(url_path[2]){
			var uid=url_path[2];
			$(document).ready(function(){
				rq_rec.get_unit({'uid':uid});
			});
		}
		
	}
	this.btn_bind=function(){

	}

	this.init();

	var re={
		'get_new_list':function(){

			var data={
				'ajax_type':'get_new'
			}
			ANK.ajax(['ajax/ajax_rq_rec.php','rq_rec','_system'],{'data':data},function(gdata){
				var d=gdata['d'];
				rq_rec.make_rq_list(d);
			},'json');
		
			window.history.pushState('RQ_REC', 'ANKat Request Records', '/system/rq_rec/');
		},
		'search_keyword':function(args){
			var keyword=$('.nav_bar input[name="keyword"]').val();
			
			$('.result .rq_list').html('');

			var data={
				'ajax_type':'get_new',
				'keyword':keyword
			}
			ANK.ajax(['ajax/ajax_rq_rec.php','rq_rec','_system'],{'data':data},function(gdata){
				var d=gdata['d'];
				rq_rec.make_rq_list(d);
			},'json');
		},
		'get_detail':function(args){
			var uid=args.attr('uid');
			var target_obj=args.next();
				target_obj.show();
			//record now opening 
			this.now_uid=uid;
			//change url
			window.history.pushState('RQ_REC', 'ANKat Request Records', '/system/rq_rec/'+uid);

			//if data had gotten
			if(target_obj.attr('status')=='ok'){
				return true;
			}
			var data={
				'ajax_type':'get_unit',
				'uid':uid
			}
			ANK.ajax(['ajax/ajax_rq_rec.php','rq_rec','_system'],{'data':data},function(gdata){
				var td=gdata['d']['d'];
				var uid=gdata['d']['uid'];
				var t_uid=uid.split('-');
				var uid_master=uid,uid_nbs;
				if(t_uid.length==2){
					uid_master=t_uid[0]+'-';
					uid_nbs=t_uid[1];
				}

				var link=window.location.origin+'/system/rq_rec/'+uid;
				target_obj.find('.uid .master').html(uid_master);
				target_obj.find('.uid .nbs').html(uid_nbs);
				target_obj.find('.link_area .link_text').val(link);
				target_obj.find('.endpoint').val(td['endpoint']);
				target_obj.find('.method').html(td['method']);
				target_obj.find('.post_data textarea').html(td['post_data']);
				target_obj.find('.raw_input textarea').html(td['raw_input']);

				target_obj.find('.server_response textarea').html(td['server_response']);

				target_obj.find('.client_headers textarea').html(td['client_headers']);
				target_obj.find('.client textarea').html(td['client']);

				target_obj.attr('status','ok');
			},'json');
		},
		//get link with uid
		'get_unit':function(d){
			var uid=d['uid'];

			var data={
				'ajax_type':'get_new',
				'keyword':uid
			}
			ANK.ajax(['ajax/ajax_rq_rec.php','rq_rec','_system'],{'data':data},function(gdata){
				var d=gdata['d'];
				rq_rec.make_rq_list(d);
				var this_obj=$('ul.rq_list li[uid="'+uid+'"]');
				rq_rec.get_detail(this_obj);
			},'json');
		},
		'hide_unit':function(args){
			var mobj=args.parents('li');
			mobj.hide();
			//change url
			window.history.pushState('RQ_REC', 'ANKat Request Records', '/system/rq_rec/');
		},
		'copy_to_clip':function(args){

			var now_url = args.parents('.link_area').find('.link_text');
			    now_url.select();
			    document.execCommand("copy");

			    alert('copy completed!');
		},
		'make_rq_list':function(d){
			var target_obj=$('.result .rq_list');
			var hidden_obj=$('.hidden_rq .rq_unit');
			target_obj.html('');
			for(var i=0,cnt=d.length;i<cnt;i++){
				var hobj=hidden_obj.clone();
				var td=d[i];
				var tt=parseInt(td['timestamp'])*1000;
				var dtime=new Date(tt);
				var dtime_str=dtime.toLocaleDateString()+' '+dtime.toLocaleTimeString();

				hobj.find('li[stype="index"]').find('.dtime').html(dtime_str);

				if(!td['track_id']){
					hobj.find('li[stype="index"]').find('.track_id').remove();
				}else{
					hobj.find('li[stype="index"]').find('.track_id').html(td['track_id']);
				}
				
				hobj.find('li[stype="index"]').find('.uid').html(td['uid']);
				hobj.find('li[stype="index"]').find('.method').html(td['method']);
				hobj.find('li[stype="index"]').find('.uri').html(td['uri']);


				hobj.find('li[stype="index"]').attr('uid',td['uid']);
				hobj.find('li[stype="content"]').attr('uid',td['uid']);
				

				hobj.find('li').appendTo(target_obj);
			}
		},
		'request_again':function(args){
			var cfm=confirm('Are you sure to request this again?');
			if(!cfm)return false;

			var mobj=args.parents('li');

			var uid=this.now_uid;
			var data={
				'ajax_type':'request_again',
				'uid':uid
			}
			
			ANK.ajax(['ajax/ajax_rq_rec.php','rq_rec','_system'],{'data':data},function(gdata){
				if(gdata['status']){
					var track_id=gdata['track_id'];
					mobj.find('.btn_rq_just').attr('track_id',track_id);
					mobj.find('.btn_rq_just').show();
					alert('request again completed!');
				}else{
					alert('Some error occurred');
				}
			});
		},
		'view_just_requested':function(args){
			var track_id=args.attr('track_id');
			$('.nav_bar input[name="keyword"]').val(track_id);
			this.search_keyword();
		}
	}

	return re;
}());

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    //json = json.replace(/\,/g, "<br>").replace(/\{/g, "{<br>").replace(/\}/g, "<br>}");
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}