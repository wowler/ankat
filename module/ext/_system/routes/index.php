<?php
namespace _system\routes;

//if debug mode is not on
$dev_status=\ANK_CONF::get('dev')['status'];
if($dev_status!==true){
	echo 'DEV MODE is off. please set dev mode on ankat_config.';
	exit;
}

//disable request records
\ANK::$rq_rec->disable();

$args=\ANK::$route->get_args();

switch ($args[0]){
	case 'db':
		require \ANK::git('index.php','db','_system');
	break;
	case 'logs':
		require \ANK::git('index.php','logs','_system');
	break;
	case 'rq_rec':
		require \ANK::git('index.php','rq_rec','_system');
	break;
	default:
		require \ANK::git('index.php','home','_system');
	break;
}
?>