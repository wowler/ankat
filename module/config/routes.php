<?php
/*
	Setting routes
*/
/*
$rt_bsc=ANK::$route->get('basic')[0];
$default_path=ROOT_PATH.'module/layouts/';
$init_file_path=$default_path.ANK_CONF::get('init_file')[0];
*/
\ANK::$route->set('/tool-make-user',function(){
	$ct_url=ANK::git('index.php','make_user','_tools');

	return $ct_url;
});
\ANK::$route->set('/tool-base64',function(){
	$ct_url=ANK::git('index.php','base64','_tools');

	return $ct_url;
});
\ANK::$route->set('/tool-unixtime',function(){
	$ct_url=ANK::git('index.php','unixtime','_tools');

	return $ct_url;
});
\ANK::$route->set('/file_test',function(){
	$ct_url=\ANK::git('index.php','file','_test');

	return $ct_url;
});
\ANK::$route->set('/fdb',function(){
	$ct_url=\ANK::git('test.php','fdb','_test');

	return $ct_url;
});
\ANK::$route->set('/arrdb',function(){
	$ct_url=\ANK::git('test.php','arrDB','_test');

	return $ct_url;
});
//SYSTEM
\ANK::$route->set('/system',function(){
	\ANK::$route->set_layout(ROOT_PATH."/module/layouts/_system.php");
	
	$ct_url=\ANK::git('index.php','routes','_system');

	return $ct_url;
});
//others static page
\ANK::$route->set('/',function($val1){
	return \ANK::git('index.php','home','_main');
	//accept types
	$default_arr=[];
	
	//if null then default
	if(strlen($val1)<1){
		$val1='index';
		$default_arr[]='index';
	}

	//if else then set consruction or 404
	// if(!in_array($val1,$default_arr)){
	// 	$val1='construct';
	// }
	$ct_url=\ANK::git("{$val1}.php",'home');

	//beta for testing
	$rd=$ct_url;//return need datas

	return $rd;
});

return ;
?>